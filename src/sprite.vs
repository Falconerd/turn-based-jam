#version 450 core
layout (location = 0) in vec3 a_vertex;
layout (location = 1) in vec4 a_colour;
layout (location = 2) in vec2 a_tex_coords;

out vec4 v_colour;
out vec2 v_tex_coords;

uniform mat4 transform;
uniform mat4 projection;

void main()
{
	gl_Position = projection * transform * vec4(a_vertex.xyz, 1.0);
	v_colour = a_colour;
	v_tex_coords = a_tex_coords;
}

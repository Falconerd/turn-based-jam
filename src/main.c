#include <assert.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <stdarg.h>
#include "ash.h"

#define DEBUG

#ifdef DEBUG
# define D(x) x
#else
# define D(x)
#endif

/* PUT ME SOMEWHERE */
void mouse_button_callback_tactics(GLFWwindow *window, int button, int action, int mods);
void mouse_button_callback_presets(GLFWwindow *window, int button, int action, int mods);
void mouse_button_callback_battle(GLFWwindow *window, int button, int action, int mods);
void key_callback_tactics(GLFWwindow *window, int key, int scancode, int action, int mods);
void key_callback_presets(GLFWwindow *window, int key, int scancode, int action, int mods);
void key_callback_game_over(GLFWwindow *window, int key, int scancode, int action, int mods);
void key_callback_battle(GLFWwindow *window, int key, int scancode, int action, int mods);
void cursor_position_callback(GLFWwindow *window, double xpos, double ypos);
void switch_scene(void (*new_scene)());
void tactics_cycle_hero();
void tactics_cycle_preset();
void scene_presets();
void scene_battle();
void scene_game_over();
struct piece *piece_from_name();
struct piece *piece_from_id();

float
fmax2(double a, double b)
{
	if (a > b)
		return a;
	else
		return b;
}

/* TYPES ==================================================================== */

#define MAXSTATUSEFFECTS 16

enum piece_return_types {
	PRT_NONE,
	PRT_ANY,
	PRT_NUMBER,
	PRT_BOOLEAN,
	PRT_VECTOR,
	PRT_VECTOR_LIST,
	PRT_UNIT
};

struct piece {
	unsigned id;
	char img[32];
	char name[32];
	char desc[128];
	char file[32];
	struct sprite *sprite;
	unsigned sprite_id;
	enum piece_return_types return_type;
	char input_names[4][32];
	enum piece_return_types input_types[4];
	unsigned input_count;
	int ap;
};

typedef unsigned piece_type;

struct socket_state {
	int p; /* stat values, bools, x part of vector */
	int q; /* y part of vector */
};

struct socket {
	piece_type type;
	unsigned input_sides[4];
	float x;
	float y;
	float w;
	float h;
	enum piece_return_types return_type; /* may change in some cases, f.e. connectors */
	struct socket_state state;
};
struct preset {
	char name[32];
	char file[32];
	struct socket sockets[64];
	unsigned piece_order[64];
	unsigned piece_count;
};

struct stat {
	int val;
	int max;
};

typedef enum {
	BLIND,
	STUN,
	IMMUNE,
	POISON,
	COVERING,
	COVERED,
	GRAPPLE
} status_effect_e;

struct status_effect {
	status_effect_e type;
	unsigned turns;
};

struct unit {
	char img[128];
	struct sprite *sprite;
	unsigned sprite_id;
	char name[32];
	int x; /* these are ints to make subtraction easier */
	int y;
	int id;
	float ox;
	float oy;
	float fx;
	float fy;
	unsigned status;
	struct stat hp;
	struct stat spd;
	struct stat ap;
	struct stat pdf;
	struct stat mdf;
	struct sprite_animated *asprite;
	int next_turn;
	unsigned preset;
	struct preset presets[3];
	unsigned sets[3];
	unsigned team;
	char strid[32];
	int charging;
	int charge_turn;
	unsigned turn;
	float p; /* can be saved to for later use */
	float q;
	unsigned telegraphs[64];
	struct status_effect status_effects[MAXSTATUSEFFECTS];
	struct unit *unit_covering;
};

enum dmg_t {
	DMG_PHS,
	DMG_MAG
};

struct battle_anim {
	float *val;
	float startval;
	float delta;
	double dur;
	float start;
	unsigned from_zero;
	float (*easing)(float);
};

/* GLOBALS ================================================================== */

#define PI 3.1415926
#define SCR_WIDTH 384
#define SCR_HEIGHT 216
#define SCR_SCALE 5
#define CLEAR_COLOUR 0.2431372f, 0.2078431f, 0.2745098f, 1.0f

#define TEAM_ALLY 1
#define TEAM_ENEMY 2

struct colour colour_white  = { 1, 1, 1, 1 };
struct colour colour_black  = { 0, 0, 0, 1 };
struct colour colour_green  = { 0, 1, 0, 1 };
struct colour colour_yellow = { 1, 1, 0, 1 };
struct colour colour_red    = { 1, 0, 0, 1 };
struct colour colour_purple = { 1, 0, 1, 1 };

GLFWwindow *window;
GLuint text_shader,	ui_shader,	sprite_shader;
GLuint text_vao,	ui_vao,		sprite_vao;
GLuint text_vbo,	ui_vbo,		sprite_vbo;
GLuint 			ui_ebo,		sprite_ebo;
FT_Library ft;
struct font fonts[32] = {0};
int font_count = 0;
mat4x4 model_normal = {
	{ 1, 0, 0, 0 },
	{ 0, 1, 0, 0 },
	{ 0, 0, 1, 0 },
	{ 0, 0, 0, 1 },
};
#define MAXSPRITES 256
struct sprite sprites[MAXSPRITES] = {0};
unsigned nsprites = 0;
#define MAXASPRITES 256
struct sprite_animated sprites_animated[MAXASPRITES] = {0};
int nasprites = 0;

double time_now = 0;
double time_last_frame = 0;
double delta_time = 0;

double cursor_x = 0;
double cursor_y = 0;

void (*scene)();
int victory = 0;

struct unit heroes[3] = {0};

#define MAXENEMIES 256
struct unit enemies[MAXENEMIES] = {0};
unsigned nenemies = 0;

#define MAXPIECES 256
struct piece pieces[MAXPIECES] = {0};
unsigned npieces = 0;

#define MAXPRESETS 256
struct preset presets[MAXPRESETS] = {0};
unsigned npresets = 0;

double shakex = 0; 
double shakey = 0;

float tint_r = 1;
float tint_g = 1;
float tint_b = 1;
float tint_a = 1;

struct colour
hex_to_rgba(int hex)
{
	struct colour c;
	c.r = ((hex >> 24) & 0xff) / 255.0;
	c.g = ((hex >> 16) & 0xff) / 255.0;
	c.b = ((hex >>  8) & 0xff) / 255.0;
	c.a = ((hex >>  0) & 0xff) / 255.0;
	return c;
}

unsigned
int_in_range(int x, int a, int b)
{
	return (x >= a && x <= b);
}

void
error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void
create_window(const char *name)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwSetErrorCallback(error_callback);

	window = glfwCreateWindow(SCR_WIDTH * SCR_SCALE, SCR_HEIGHT * SCR_SCALE, name, NULL, NULL);

	assert(window);

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetKeyCallback(window, key_callback);

	assert(gladLoadGLLoader((GLADloadproc)glfwGetProcAddress));
}

void
create_text_shader()
{
	int i = 0;
	mat4x4 projection;

	text_shader = create_shader_program("src/glyph.vs", "src/glyph.fs", NULL);
	glUseProgram(text_shader);
	mat4x4_ortho(projection, 0, SCR_WIDTH * SCR_SCALE, 0, SCR_HEIGHT * SCR_SCALE, -1.0, 1.0);
	setUniformMatrix4fv(&text_shader, "projection", &projection[0][0]);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if (FT_Init_FreeType(&ft)) {
		D(printf("failed to init freetype\n"));
		exit(-1);
	}

	for (i = 0; i < font_count; ++i) {
		GLubyte c = 0;
		int g = 0;
		FT_Face face;

		if (FT_New_Face(ft, fonts[i].path, 0, &face)) {
			D(printf("failed to load %s\n", fonts[i].path));
			exit(-1);
		}

		FT_Set_Pixel_Sizes(face, 0, fonts[i].size);

		for (c = 0; c < 128; ++c) {
			GLuint texture;

			if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
				D(printf("Failed to load glyph '%c'\n", c));
				continue;
			}

			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D,
				     0,
				     GL_RED,
				     face->glyph->bitmap.width,
				     face->glyph->bitmap.rows,
				     0,
				     GL_RED,
				     GL_UNSIGNED_BYTE,
				     face->glyph->bitmap.buffer);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			fonts[i].glyphs[g].texture_id = texture;
			fonts[i].glyphs[g].width = face->glyph->bitmap.width;
			fonts[i].glyphs[g].height = face->glyph->bitmap.rows;
			fonts[i].glyphs[g].bearing_left = face->glyph->bitmap_left;
			fonts[i].glyphs[g].bearing_top = face->glyph->bitmap_top;
			fonts[i].glyphs[g].advance = face->glyph->advance.x;

			++g;
		}
		glBindTexture(GL_TEXTURE_2D, 0);
		FT_Done_Face(face);
	}

	FT_Done_FreeType(ft);

	glGenVertexArrays(1, &text_vao);
	glGenBuffers(1, &text_vbo);
	glBindVertexArray(text_vao);
	glBindBuffer(GL_ARRAY_BUFFER, text_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void
create_ui_shader()
{
	mat4x4 projection;
	mat4x4 model;
	GLfloat vertices[] = {
		1.0f,  1.0f, 0.0f,
		1.0f, -0.0f, 0.0f,
		-0.0f, -0.0f, 0.0f,
		-0.0f,  1.0f, 0.0f,
	};
	GLint indices[] = {
		0, 1, 3,
		1, 2, 3
	};
	ui_shader = create_shader_program("src/ui.vs", "src/ui.fs", NULL);
	mat4x4_add(projection, projection, model_normal);
	mat4x4_add(model, model, model_normal);

	glGenVertexArrays(1, &ui_vao);
	glGenBuffers(1, &ui_vbo);
	glGenBuffers(1, &ui_ebo);

	glBindVertexArray(ui_vao);
	glBindBuffer(GL_ARRAY_BUFFER, ui_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ui_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	mat4x4_ortho(projection, 0, SCR_WIDTH, 0, SCR_HEIGHT, -1.0f, 1.0f);

	glUseProgram(ui_shader);

	setUniformMatrix4fv(&ui_shader, "projection", &projection[0][0]);
	setUniformMatrix4fv(&ui_shader, "model", &model[0][0]);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void
create_sprite_shader()
{
	mat4x4 projection;
	GLfloat vertices[] = {
		/* vertices		colours				tex coords */
		0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 1.0f,
		0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 0.0f,
		-0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 1.0f
	};
	GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	glGenVertexArrays(1, &sprite_vao);
	glGenBuffers(1, &sprite_vbo);
	glGenBuffers(1, &sprite_ebo);

	glBindVertexArray(sprite_vao);

	glBindBuffer(GL_ARRAY_BUFFER, sprite_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	sprite_shader = create_shader_program("src/sprite.vs", "src/sprite.fs", NULL);

	glUseProgram(sprite_shader);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(7 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	mat4x4_ortho(projection, 0, SCR_WIDTH, 0, SCR_HEIGHT, -1.0f, 1.0f);
	setUniformMatrix4fv(&sprite_shader, "projection", &projection[0][0]);
}

GLfloat
render_text(const char *text, struct font *font, GLfloat x, GLfloat y, struct colour colour)
{
	int i;
	int len = strlen(text);

	x *= SCR_SCALE;
	y *= SCR_SCALE;

	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(text_shader);
	setUniform4f(&text_shader, "text_colour", colour.r, colour.g, colour.b, colour.a);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(text_vao);

	for (i = 0; i < len; ++i) {
		struct glyph g = font->glyphs[(int)text[i]];

		float xpos = x + g.bearing_left;
		float ypos = y - (g.height - g.bearing_top);

		float w = g.width;
		float h = g.height;

		float vertices[6][4] = {
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 1 },
			{ 0, 0, 1, 1 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 1, 1 },
			{ 0, 0, 1, 0 },
		};

		vertices[0][0] = xpos;
		vertices[0][1] = ypos + h;

		vertices[1][0] = xpos;
		vertices[1][1] = ypos;

		vertices[2][0] = xpos + w;
		vertices[2][1] = ypos;

		vertices[3][0] = xpos;
		vertices[3][1] = ypos + h;

		vertices[4][0] = xpos + w;
		vertices[4][1] = ypos;

		vertices[5][0] = xpos + w;
		vertices[5][1] = ypos + h;

		glBindTexture(GL_TEXTURE_2D, g.texture_id);
		glBindBuffer(GL_ARRAY_BUFFER, text_vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof vertices, vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		x += (g.advance >> 6);
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);

	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	return x;
}

void
render_text_wrapped(const char *text, struct font *font, GLfloat x, GLfloat y, struct colour colour, GLfloat width, GLfloat height)
{
	char str[4096];
	char delimiter[2] = " ";
	char *token;
	GLfloat cursor_x = x;
	GLfloat cursor_y = y;

	strcpy(str, text);

	token = strtok(str, delimiter);

	while (NULL != token) {
		int len = strlen(token);

		if (cursor_x + (font->size / 2) * len > x + width) {
			cursor_x = x;
			cursor_y -= font->size * font->line_height;
			continue;
		}

		if (cursor_y < y - height) {
			break;
		}

		cursor_x = render_text(" ", font, cursor_x, cursor_y, colour);
		cursor_x = render_text(token, font, cursor_x, cursor_y, colour);
		token = strtok(NULL, delimiter);
	}
}

void
render_quad(struct colour *colour, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height)
{
	mat4x4 model;
	mat4x4_add(model, model, model_normal);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(ui_shader);
	glBindVertexArray(ui_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ui_ebo);

	mat4x4_translate(model, x, y, z);
	mat4x4_scale_aniso(model, model, width, height, 1);

	setUniformMatrix4fv(&ui_shader, "model", &model[0][0]);
	setUniform4f(&ui_shader, "colour", colour->r, colour->g, colour->b, colour->a);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glDisable(GL_BLEND);
}

void
renderer_init()
{
	stbi_set_flip_vertically_on_load(1);
	create_window("ASH TEST");
	glClearColor(CLEAR_COLOUR);
	create_text_shader();
	create_ui_shader();
	create_sprite_shader();
	glfwSetCursorPosCallback(window, cursor_position_callback);
}

void
font_add(const char *path, int size, float line_height)
{
	fonts[font_count].path = path;
	fonts[font_count].size = size;
	fonts[font_count].line_height = line_height;
	font_count++;
}

unsigned
sprite_add(const char *path)
{
	int w, h, nc;
	int id = nsprites;
	unsigned char *data = stbi_load(path, &w, &h, &nc, 0);

	if (NULL == data) {
		D(printf("failed to load texture %s\n", path));
		exit(-1);
	}

	glUseProgram(sprite_shader);
	glBindVertexArray(sprite_vao);
	glBindBuffer(GL_ARRAY_BUFFER, sprite_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_vao);

	glGenTextures(1, &sprites[id].texture_id);
	glBindTexture(GL_TEXTURE_2D, sprites[id].texture_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);

	sprites[id].width = w;
	sprites[id].height = h;

	nsprites++;

	return id;
}

void
render_sprite(struct sprite *sprite)
{
	mat4x4 transform;

	/* cullface makes sprites invis */
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(sprite_shader);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(sprite_vao);

	mat4x4_identity(transform);
	mat4x4_translate(transform, sprite->x + shakex, sprite->y + shakey, sprite->z);
	mat4x4_rotate(transform, transform, 0, 0, 1.0f, sprite->rot);
	mat4x4_scale_aniso(transform, transform, sprite->height, sprite->width, 1.0f);
	setUniformMatrix4fv(&sprite_shader, "transform", &transform[0][0]);
	setUniform4f(&sprite_shader, "tint", tint_r, tint_g, tint_b, tint_a);

	glBindTexture(GL_TEXTURE_2D, sprite->texture_id);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
	glDisable(GL_BLEND);
}

void
render_sprite_animated(struct sprite_animated *asprite, GLfloat x, GLfloat y, GLfloat z, GLfloat rot)
{
	int aid = asprite->index;
	int fid = asprite->animations[aid].index;
	unsigned sid = asprite->animations[aid].indices[fid];
	struct sprite *sprite = &sprites[sid];
	sprite->x = x;
	sprite->y = y;
	sprite->z = z;
	sprite->rot = rot;
	render_sprite(sprite);
}

/* client code below */

struct sprite_animated player_sprite = {0};

void
framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

/* TACTICS ================================================================== */

unsigned scene_tactics_initialised = 0;

/* @TODO: set piece sprite ids ? */
unsigned piece_sprite_ids[MAXPIECES] = {0};

unsigned tactics_hero_id = 0;
unsigned tactics_enemy_id = 0;
unsigned tactics_enemy_edit = 0;

char *
input_type_to_name(enum piece_return_types type)
{
	switch (type) {
	case PRT_NUMBER:
		return "Number";
	case PRT_BOOLEAN:
		return "Boolean";
	case PRT_VECTOR:
		return "Vector";
	case PRT_VECTOR_LIST:
		return "Vector List";
	case PRT_ANY:
		return "Any";
	case PRT_UNIT:
		return "Unit";
	case PRT_NONE:
		return "None";
	}
	return "NULL";
}



char *
piece_type_to_string(piece_type type)
{
	return pieces[type].name;
}

struct socket sockets[64] = {0};

unsigned sprite_id_tactics_box;
unsigned sprite_id_tactics_buttons;
unsigned sprite_id_input_base;
unsigned sprite_id_input_green_base;
unsigned sprite_id_input_blue_base;
unsigned sprite_id_input_red_base;
unsigned sprite_id_input_green;
unsigned sprite_id_input_blue;
unsigned sprite_id_input_red;
unsigned sprite_id_input_arrow_green;
unsigned sprite_id_input_arrow_blue;
unsigned sprite_id_input_arrow_red;
unsigned base_input_sprite_ids[3];
unsigned input_sprite_ids[3];
unsigned input_arrow_sprite_ids[3];
piece_type cursor_piece = 0;

unsigned sprite_id_pagination_1;
unsigned sprite_id_pagination_1_selected;
unsigned sprite_id_pagination_2;
unsigned sprite_id_pagination_2_selected;
unsigned sprite_id_pagination_3;
unsigned sprite_id_pagination_3_selected;
unsigned sprite_id_pagination_4;
unsigned sprite_id_pagination_4_selected;
unsigned sprite_id_pagination_5;
unsigned sprite_id_pagination_5_selected;

unsigned sprite_id_button_battle;
unsigned sprite_id_button_done;
unsigned sprite_id_button_presets;

unsigned sprite_id_button_preset_a;
unsigned sprite_id_button_preset_b;
unsigned sprite_id_button_preset_c;

unsigned debug_square_id;
unsigned piece_highlight_id;

int selected_socket = -1;
unsigned selected_piece = 0;
unsigned piece_page = 1;
unsigned page_count = 5;

#define INPUTS_X 300
#define INPUTS_Y (SCR_HEIGHT - 50)

#define PAGINATION_X 15
#define PAGINATION_Y 139

#define INPUT_SIDE_NONE 0
#define INPUT_SIDE_TOP 3
#define INPUT_SIDE_RIGHT 2
#define INPUT_SIDE_BOTTOM 1
#define INPUT_SIDE_LEFT 4


#define SOCKET_ID_EMPTY 99

struct tacgraph_node {
	unsigned socket_id;
	struct tacgraph_node *next;
};

struct tacgraph_list {
	unsigned socket_id;
	struct tacgraph_node *head;
};

struct tacgraph {
	unsigned count;
	struct tacgraph_list *list;
};

struct tacgraph_list *
tacgraph_list_by_socket_id(struct tacgraph *g, unsigned socket_id)
{
	unsigned i;

	for (i = 0; i < g->count; ++i) {
		if (g->list[i].socket_id == socket_id)
			return &g->list[i];
	}

	return NULL;
}

void
tacgraph_dfs(unsigned socket_id, unsigned *V, unsigned *visited, unsigned *vn, struct tacgraph *g)
{
	unsigned i;
	struct tacgraph_node *curr;
	V[socket_id] = 1;
	/* CANT USE SOCKET ID IN g->list, NEED TO FIND INDEX */
	for (i = 0; i < g->count; ++i) {
		if (g->list[i].socket_id == socket_id)
			break;
	}
	curr = g->list[i].head;
	if (curr != NULL) {
	while (curr != NULL) {
		D(printf("Checking socket %u\n", curr->socket_id));
		if (V[curr->socket_id] == 0) {
			tacgraph_dfs(curr->socket_id, V, visited, vn, g);
		}
		curr = curr->next;
	}
	}

	D(printf("Visited %u. vn: %u\n", socket_id, *vn));
	visited[*vn] = socket_id;
	*vn = *vn + 1;
}

void
tacgraph_topsort(struct tacgraph *g, unsigned *list)
{
	unsigned V[64] = {0};
	unsigned i, j, c = 0;
	unsigned vn = 0;

	for (i = 0; i < g->count; ++i) {
		unsigned socket_id = g->list[i].socket_id;
		if (V[socket_id] == 0) {
			unsigned visited[64] = {0};
			tacgraph_dfs(socket_id, V, visited, &vn, g);
			for (j = 0; j < vn; ++j) {
				list[c] = visited[j];
				c++;
			}
			vn = 0;
		}
	}
}

/* @TODO: Rewrite this. It is really inefficient.
 * low priority tho */
struct tacgraph *
tacgraph_create(struct socket sockets[64])
{
	unsigned i, j;
	struct tacgraph *g = malloc(sizeof(struct tacgraph));
	unsigned n = 0;

	/* count nodes */
	for (i = 0; i < 64; ++i) {
		if (sockets[i].type != 0)
			++n;
	}

	g->count = n;

	/* create adjacency lists */
	g->list = malloc(n * sizeof(struct tacgraph_list));

	/* set each list defaults */
	for (i = 0; i < n; ++i) {
		g->list[i].head = NULL;
		g->list[i].socket_id = SOCKET_ID_EMPTY;
	}

	/* set each node to a head */
	for (i = 0; i < 64; ++i) {
		if (sockets[i].type == 0)
			continue;
		for (j = 0; j < n; ++j) {
			if (g->list[j].socket_id == SOCKET_ID_EMPTY) {
				g->list[j].socket_id = i;
				break;
			}
		}
	}

	for (i = 0; i < n; ++i) {
		D(printf(":: %d ", g->list[i].socket_id));
	}
	D(printf("\n"));

	/* add nodes to each list */
	for (i = 0; i < 64; ++i) {
		for (j = 0; j < 4; ++j) {
			struct tacgraph_node *node = NULL;
			struct tacgraph_list *list = NULL;
			int id = -1;
			switch (sockets[i].input_sides[j]) {
			case INPUT_SIDE_TOP: {
				D(printf("TOP\n"));
				id = i - 8;
			} break;
			case INPUT_SIDE_RIGHT: {
				D(printf("R\n"));
				id = i + 1;
			} break;
			case INPUT_SIDE_BOTTOM: {
				D(printf("B\n"));
				id = i + 8;
			} break;
			case INPUT_SIDE_LEFT: {
				D(printf("L\n"));
				id = i - 1;
			} break;
			}
			if (id != -1) {
				if (sockets[id].type == 0) {
					D(printf("Edge to empty piece %d\n", id));
					break;
				}
				node = malloc(sizeof(struct tacgraph_node));
				list = tacgraph_list_by_socket_id(g, i);
				node->next = list->head;
				node->socket_id = id;
				list->head = node;
			}
 		}
	}

	return g;
}

void
tacgraph_print(struct tacgraph *g)
{
	unsigned i;

	for (i = 0; i < g->count; ++i) {
		struct tacgraph_node *curr = g->list[i].head;
		D(printf("%d <-", g->list[i].socket_id));
		while (curr != NULL) {
			D(printf(" %d", curr->socket_id));
			curr = curr->next;
		}
		D(printf("\n"));
	}
}

void
tacgraph_free(struct tacgraph *g)
{
	unsigned i;

	for (i = 0; i < g->count; ++i) {
		struct tacgraph_node *curr = g->list[i].head;
		struct tacgraph_node *prev = NULL;
		while (curr != NULL) {
			prev = curr;
			curr = curr->next;
			free(prev);
		}
	}

	free(g->list);
	free(g);
}

struct tacgraph *tg_test;

void
load_unit_preset(struct preset *p)
{
	unsigned i, j;
	for (i = 0; i < 64; ++i) {
		sockets[i].type = p->sockets[i].type;
		sockets[i].return_type = p->sockets[i].return_type;
		for (j = 0; j < 4; ++j)
			sockets[i].input_sides[j] = p->sockets[i].input_sides[j];
	}
}

void
load_enemy_tactics(unsigned id)
{
	struct unit *e = &enemies[id];
	if (NULL == e) {
		D(printf("Enemy with id %d is NULL. Exiting.\n", id));
		exit(1);
	} else {
		load_unit_preset(&e->presets[0]);
	}
}

void load_hero_tactics(unsigned id)
{
	struct unit *h = &heroes[id];
	if (NULL == h) {
		D(printf("Hero with id %d is NULL. Exiting.\n", id));
		exit(1);
	} else {
		load_unit_preset(&h->presets[0]);
	}
}

void
preset_set_by_filename(struct preset *p, const char *filename)
{
	unsigned i, j;
	for (i = 0; i < npresets; ++i) {
		D(printf("%s : %s\n", presets[i].file, filename));
		if (0 == strcmp(presets[i].file, filename)) {
			D(printf("Found %s\n", filename));
			*p = presets[i];
			for (j = 0; j < 64; ++j) {
				p->piece_order[j] = presets[i].piece_order[j];
				p->piece_count = presets[i].piece_count;
				p->sockets[j] = presets[i].sockets[j];
				p->sockets[j].type = presets[i].sockets[j].type;
				p->sockets[j].return_type = presets[i].sockets[j].return_type;
			}
		}
	}
}

void
key_callback_tactics(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS) {
		switch (key) {
		case GLFW_KEY_B: {
			tactics_cycle_hero();
		} break;
		case GLFW_KEY_V: {
			tactics_cycle_preset();
		} break;
		case GLFW_KEY_D: {
			switch_scene(scene_presets);
		} break;
		case GLFW_KEY_SEMICOLON: {
			if (1 == tactics_enemy_edit) {
				load_hero_tactics(tactics_hero_id);
				tactics_enemy_edit = 0;
			} else {
				load_enemy_tactics(tactics_enemy_id);
				tactics_enemy_edit = 1;
			}
		} break;
		}
	}
}

void
clear_inputs(unsigned socket_id)
{
	unsigned i = socket_id, j;
	if (i > 7) {
		for (j = 0; j < 4; ++j) {
			if (sockets[i - 8].input_sides[j] == INPUT_SIDE_BOTTOM)
				sockets[i - 8].input_sides[j] = INPUT_SIDE_NONE;
		}
	}
	if (i < 56) {
		for (j = 0; j < 4; ++j) {
			if (sockets[i + 8].input_sides[j] == INPUT_SIDE_TOP)
				sockets[i + 8].input_sides[j] = INPUT_SIDE_NONE;
		}
	}
	if (i % 8 > 0) {
		for (j = 0; j < 4; ++j) {
			if (sockets[i - 1].input_sides[j] == INPUT_SIDE_RIGHT)
				sockets[i - 1].input_sides[j] = INPUT_SIDE_NONE;
		}
	}
	if (i % 8 < 7) {
		for (j = 0; j < 4; ++j) {
			if (sockets[i + 1].input_sides[j] == INPUT_SIDE_LEFT)
				sockets[i + 1].input_sides[j] = INPUT_SIDE_NONE;
		}
	}
}

void
insert_piece(piece_type type, struct socket *socket)
{
	socket->type = type;
	socket->return_type = pieces[type].return_type;
	socket->state.p = 0;
	socket->state.q = 0;
	socket->input_sides[0] = INPUT_SIDE_NONE;
	socket->input_sides[1] = INPUT_SIDE_NONE;
	socket->input_sides[2] = INPUT_SIDE_NONE;
	socket->input_sides[3] = INPUT_SIDE_NONE;
}

void
scene_tactics_init()
{
	unsigned i, row, col;

	glfwSetMouseButtonCallback(window, mouse_button_callback_tactics);
	glfwSetKeyCallback(window, key_callback_tactics);

	if (scene_tactics_initialised)
		return;

	base_input_sprite_ids[0] = sprite_id_input_blue_base;
	base_input_sprite_ids[1] = sprite_id_input_green_base;
	base_input_sprite_ids[2] = sprite_id_input_red_base;

	input_sprite_ids[0] = sprite_id_input_blue;
	input_sprite_ids[1] = sprite_id_input_green;
	input_sprite_ids[2] = sprite_id_input_red;

	input_arrow_sprite_ids[0] = sprite_id_input_arrow_blue;
	input_arrow_sprite_ids[1] = sprite_id_input_arrow_green;
	input_arrow_sprite_ids[2] = sprite_id_input_arrow_red;

	/* @NOTE: 64 is from 8x8 grid */
	for (i = 0; i < 64; ++i) {
		struct sprite *sprite = &sprites[piece_sprite_ids[0]];
		/* @NOTE: magic number 14 and 96 is referenced from the mockup image */
		col = i % 8;
		row = i / 8;
		sockets[i].type = 0;
		sockets[i].input_sides[0] = INPUT_SIDE_NONE;
		sockets[i].input_sides[1] = INPUT_SIDE_NONE;
		sockets[i].input_sides[2] = INPUT_SIDE_NONE;
		sockets[i].input_sides[3] = INPUT_SIDE_NONE;
		sockets[i].x = 96 + col * sprite->width + sprite->width / 2;
		sockets[i].y = SCR_HEIGHT - (14 + row * sprite->height + sprite->height / 2);
		sockets[i].w = sprite->width;
		sockets[i].h = sprite->height;
	}

	sprite_id_tactics_box = sprite_add("assets/tactics_piece_box.png");

	for (i = 0; i < 64; ++i) {
		sockets[i].type = heroes[tactics_hero_id].presets[0].sockets[i].type;
		sockets[i].return_type = pieces[sockets[i].type].return_type;
		sockets[i].input_sides[0] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[0];
		sockets[i].input_sides[1] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[1];
		sockets[i].input_sides[2] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[2];
		sockets[i].input_sides[3] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[3];
	}

	scene_tactics_initialised = 1;
}

void
scene_tactics_render_pieces(unsigned start)
{
	unsigned i, col, row;
	struct sprite *sprite;
	unsigned end = start + 15;

	if (end > npieces)
		end = npieces;

	/* @TODO magic number from mockup */
	for (i = start; i < end; ++i) {
		col = (i - 1) % 3;
		row = ((i - 1) / 3) % 5;
		sprite = &sprites[piece_sprite_ids[i]];
		sprite->x = 12 + col * sprite->width + sprite->width / 2;
		sprite->y = SCR_HEIGHT - (86 + row * sprite->height + sprite->height / 2);
		render_sprite(sprite);
	}
}

void
render_piece_text(struct piece *data)
{
	char s[64];
	char ap[15];
	render_text(data->name, &fonts[0], 300, SCR_HEIGHT - 16, colour_white);
	sprintf(s, "Return Type: %s", input_type_to_name(data->return_type));
	render_text(s, &fonts[1], 300, SCR_HEIGHT - 24, colour_white);
	render_text(data->desc, &fonts[1], 300, SCR_HEIGHT - 28, colour_white);
	if (data->ap > 0) {
		sprintf(ap, "AP Cost: %d", data->ap);
		render_text(ap, &fonts[1], 330, SCR_HEIGHT - 24, colour_white);
	}
}

void
scene_tactics()
{
	unsigned i;
	struct sprite *sprite = NULL;
	struct unit *hero = &heroes[tactics_hero_id];

	if (tactics_enemy_edit)
		hero = &enemies[tactics_enemy_id];

	/* render pieces and sockets
	 * @NOTE: 64 is from 8x8 grid */
	for (i = 0; i < 64; ++i) {
		struct socket *socket = &sockets[i];
		sprite = &sprites[piece_sprite_ids[socket->type]];
		sprite->x = socket->x;
		sprite->y = socket->y;
		render_sprite(sprite);
	}

	sprite = &sprites[sprite_id_tactics_box];
	/* @TODO: figure out how to render non squares */
	sprite->x = 48;
	sprite->y = SCR_HEIGHT - sprite->height / 2;
	render_sprite(sprite);

	/* render pagination
	 * @TODO make new pagination, this is not maintainable
	 */
	if (piece_page == 1) {
		sprite = &sprites[sprite_id_pagination_1_selected];
		sprite->x = PAGINATION_X;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_2];
		sprite->x = PAGINATION_X + 10;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_3];
		sprite->x = PAGINATION_X + 20;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_4];
		sprite->x = PAGINATION_X + 30;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_5];
		sprite->x = PAGINATION_X + 40;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		scene_tactics_render_pieces(1);
	} else if (piece_page == 2) {
		sprite = &sprites[sprite_id_pagination_1];
		sprite->x = PAGINATION_X;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_2_selected];
		sprite->x = PAGINATION_X + 10;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_3];
		sprite->x = PAGINATION_X + 20;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_4];
		sprite->x = PAGINATION_X + 30;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_5];
		sprite->x = PAGINATION_X + 40;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		scene_tactics_render_pieces(16);
	} else if (piece_page == 3) {
		sprite = &sprites[sprite_id_pagination_1];
		sprite->x = PAGINATION_X;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_2];
		sprite->x = PAGINATION_X + 10;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_3_selected];
		sprite->x = PAGINATION_X + 20;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_4];
		sprite->x = PAGINATION_X + 30;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_5];
		sprite->x = PAGINATION_X + 40;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		scene_tactics_render_pieces(31);
	} else if (piece_page == 4) {
		sprite = &sprites[sprite_id_pagination_1];
		sprite->x = PAGINATION_X;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_2];
		sprite->x = PAGINATION_X + 10;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_3];
		sprite->x = PAGINATION_X + 20;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_4_selected];
		sprite->x = PAGINATION_X + 30;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_5];
		sprite->x = PAGINATION_X + 40;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		scene_tactics_render_pieces(46);
	} else if (piece_page == 5) {
		sprite = &sprites[sprite_id_pagination_1];
		sprite->x = PAGINATION_X;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_2];
		sprite->x = PAGINATION_X + 10;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_3];
		sprite->x = PAGINATION_X + 20;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_4];
		sprite->x = PAGINATION_X + 30;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		sprite = &sprites[sprite_id_pagination_5_selected];
		sprite->x = PAGINATION_X + 40;
		sprite->y = PAGINATION_Y;
		render_sprite(sprite);

		scene_tactics_render_pieces(61);
	}

	if (cursor_piece) {
		sprite = &sprites[piece_sprite_ids[cursor_piece]];
		sprite->x = cursor_x;
		sprite->y = cursor_y;
		render_sprite(sprite);
	}

	if (selected_piece && !cursor_piece) {
		struct sprite *piece_sprite = &sprites[piece_sprite_ids[selected_piece]];
		struct piece *data = &pieces[selected_piece];
		sprite = &sprites[piece_highlight_id];
		sprite->x = piece_sprite->x;
		sprite->y = piece_sprite->y;
		render_sprite(sprite);

		for (i = 0; i < data->input_count; ++i) {
			struct sprite *input_sprite = &sprites[base_input_sprite_ids[i]];
			float x, y;
			x = INPUTS_X + input_sprite->width / 2;
			y = INPUTS_Y - (i * input_sprite->height);
			input_sprite->x = x;
			input_sprite->y = y;
			render_sprite(input_sprite);
			render_text(data->input_names[i], &fonts[1], x + input_sprite->width, y, colour_white);
			render_text(input_type_to_name(data->input_types[i]), &fonts[1], x + input_sprite->width, y - 4, colour_white);
		}

		render_piece_text(&pieces[selected_piece]);
	}

	if (selected_socket >= 0) {
		struct socket *socket = &sockets[selected_socket];
		struct piece *data = &pieces[socket->type];

		for (i = 0; i < data->input_count; ++i) {
			struct sprite *input_sprite = &sprites[base_input_sprite_ids[i]];
			float x, y;
			if (socket->input_sides[i] != INPUT_SIDE_NONE) {
				input_sprite = &sprites[input_sprite_ids[i]];
				input_sprite->rot = (90 * socket->input_sides[i]) * (PI/180.f);
			}
			x = INPUTS_X + input_sprite->width / 2;
			y = INPUTS_Y - (i * input_sprite->height);
			input_sprite->x = x;
			input_sprite->y = y;
			render_sprite(input_sprite);
			render_text(data->input_names[i], &fonts[1], x + input_sprite->width, y, colour_white);
			render_text(input_type_to_name(data->input_types[i]), &fonts[1], x + input_sprite->width, y - 4, colour_white);
		}

		sprite = &sprites[piece_highlight_id];
		sprite->x = socket->x;
		sprite->y = socket->y;
		render_sprite(sprite);

		render_piece_text(data);
	}

	/* render input arrows
	 * @NOTE: 64 is from 8x8 grid */
	for (i = 0; i < 64; ++i) {
		struct socket *socket = &sockets[i];
		struct piece *data = &pieces[socket->type];

		unsigned j = 0;

		/* render input arrows */
		for (j = 0; j < data->input_count; ++j) {
			if (socket->input_sides[j] != INPUT_SIDE_NONE) {
				struct sprite *arrow_sprite = &sprites[input_arrow_sprite_ids[j]];
				arrow_sprite->rot = (90 * socket->input_sides[j]) * (PI/180.f);
				arrow_sprite->x = socket->x;
				arrow_sprite->y = socket->y;
				switch (socket->input_sides[j]) {
				case INPUT_SIDE_TOP:
					arrow_sprite->y += 12;
					break;
				case INPUT_SIDE_RIGHT:
					arrow_sprite->x += 12;
					break;
				case INPUT_SIDE_BOTTOM:
					arrow_sprite->y -= 12;
					break;
				case INPUT_SIDE_LEFT:
					arrow_sprite->x -= 12;
					break;
				}
				render_sprite(arrow_sprite);
			}
		}
	}

	/* render hero info */
	render_text(hero->name, &fonts[0], 20, SCR_HEIGHT - 16, colour_white);
	render_text(hero->presets[hero->preset].name, &fonts[0], 20, SCR_HEIGHT - 26, colour_white);

	/* render presets button...
	 * menu -> tactics -> presets -> battle 
	 */
	sprite = &sprites[sprite_id_button_done];
	sprite->x = 334;
	sprite->y = 20;
	render_sprite(sprite);

	/* render_sprite(&sprites[debug_square_id]); */
}

void
process_input(GLFWwindow *window)
{
	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE))
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void
preset_to_graph(struct preset *preset)
{
	struct tacgraph *tg = tacgraph_create(preset->sockets);
	unsigned list[64] = {0};
	unsigned i;
	tacgraph_topsort(tg, list);
	for (i = 0; i < tg->count; ++i) {
		D(printf("%u -> ", *(list+i)));
	}
	D(printf("\n"));
	for (i = 0; i < 64; ++i) {
		preset->piece_order[i] = list[i];
	}
	preset->piece_count = tg->count;
	tacgraph_free(tg);
}

/* Run when any change is made to the board, displays any errors with the logic */
void
tactics_board_check()
{
	struct unit *unit = &heroes[tactics_hero_id];

	if (tactics_enemy_edit)
		unit = &enemies[tactics_enemy_id];

	preset_to_graph(&unit->presets[unit->preset]);
}

/* @TODO DRY these up */

void
tactics_cycle_hero()
{
	unsigned i;

	if (tactics_enemy_edit) {
		tactics_enemy_id++;
		if (tactics_enemy_id == nenemies)
			tactics_enemy_id = 0;

		selected_socket = -1;

		/* @TODO Add other fields */
		for (i = 0; i < 64; ++i) {
			sockets[i].type = enemies[tactics_enemy_id].presets[0].sockets[i].type;
			sockets[i].return_type = enemies[tactics_enemy_id].presets[0].sockets[i].return_type;
			sockets[i].input_sides[0] = enemies[tactics_enemy_id].presets[0].sockets[i].input_sides[0];
			sockets[i].input_sides[1] = enemies[tactics_enemy_id].presets[0].sockets[i].input_sides[1];
			sockets[i].input_sides[2] = enemies[tactics_enemy_id].presets[0].sockets[i].input_sides[2];
			sockets[i].input_sides[3] = enemies[tactics_enemy_id].presets[0].sockets[i].input_sides[3];
		}
	} else {
		tactics_hero_id++;
		if (tactics_hero_id == 3)
			tactics_hero_id = 0;

		selected_socket = -1;

		/* @TODO Add other fields */
		for (i = 0; i < 64; ++i) {
			sockets[i].type = heroes[tactics_hero_id].presets[0].sockets[i].type;
			sockets[i].return_type = heroes[tactics_hero_id].presets[0].sockets[i].return_type;
			sockets[i].input_sides[0] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[0];
			sockets[i].input_sides[1] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[1];
			sockets[i].input_sides[2] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[2];
			sockets[i].input_sides[3] = heroes[tactics_hero_id].presets[0].sockets[i].input_sides[3];
		}
	}
}

void
tactics_cycle_preset()
{
	unsigned i;
	struct unit *u = NULL;
	struct preset *p = NULL;

	if (tactics_enemy_edit)
		u = &enemies[tactics_enemy_id];
	else
		u = &heroes[tactics_hero_id];
	u->preset++;
	if (u->preset == 3)
		u->preset = 0;

	p = &u->presets[u->preset];

	selected_socket = -1;

	/* @TODO: add other fields */
	for (i = 0; i < 64; ++i) {
		sockets[i].type = p->sockets[i].type;
		sockets[i].return_type = p->sockets[i].return_type;
		sockets[i].input_sides[0] = p->sockets[i].input_sides[0];
		sockets[i].input_sides[1] = p->sockets[i].input_sides[1];
		sockets[i].input_sides[2] = p->sockets[i].input_sides[2];
		sockets[i].input_sides[3] = p->sockets[i].input_sides[3];
	}
}

void
key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
		switch_scene(scene == scene_tactics ? scene_battle : scene_tactics);

	if (key == GLFW_KEY_D && action == GLFW_PRESS) {
		sprites_animated[0].index = sprites_animated[0].index ? 0 : 1;
	}

}

/* return 0 if OK
 * check if invalid side (edges)
 * check if side already used by another input
 * check if side used by other socket
 * ckeck if correct type
 */
unsigned
tactics_input_selection_check(unsigned socket_id, unsigned input, unsigned side)
{
	unsigned i;
	struct socket *socket = &sockets[socket_id];
	struct socket *socket_adj;
	unsigned connector_id = piece_from_name("Connector")->id;
	unsigned none_id = piece_from_name("None")->id;

	/* used by another input of this socket */
	for (i = 0; i < 4; ++i) {
		if (i == input) continue;
		if (socket->input_sides[i] == side)
			return 2;
	}

	switch (side) {
	case INPUT_SIDE_BOTTOM:
		/* edge */
		if (socket_id > 55)
			return 1;
		/* used by adjacent */
		socket_adj = &sockets[socket_id + 8];
		for (i = 0; i < 4; ++i) {
			if (socket_adj->input_sides[i] == INPUT_SIDE_TOP)
				return 3;
		}
		break;
	case INPUT_SIDE_RIGHT:
		/* edge */
		if ((socket_id+1) % 8 == 0)
			return 1;
		/* used by adjacent */
		socket_adj = &sockets[socket_id + 1];
		for (i = 0; i < 4; ++i) {
			if (socket_adj->input_sides[i] == INPUT_SIDE_LEFT)
				return 3;
		}
		break;
	case INPUT_SIDE_TOP:
		/* edge */
		if (socket_id < 8)
			return 1;
		/* used by adjacent */
		socket_adj = &sockets[socket_id - 8];
		for (i = 0; i < 4; ++i) {
			if (socket_adj->input_sides[i] == INPUT_SIDE_BOTTOM)
				return 3;
		}
		break;
	case INPUT_SIDE_LEFT:
		/* edge */
		if ((socket_id) % 8 == 0)
			return 1;
		/* used by adjacent */
		socket_adj = &sockets[socket_id - 1];
		for (i = 0; i < 4; ++i) {
			if (socket_adj->input_sides[i] == INPUT_SIDE_RIGHT)
				return 3;
		}
		break;
	}


	/* if connector, set the return type */
	if (socket->type == connector_id) {
		struct socket *s;

		/* empty type */
		if (socket_adj->type == none_id)
			return 7;

		socket->return_type = socket_adj->return_type;


		/* if connector and adj is connector, must already have reture type */
		if (socket_adj->type == connector_id && socket_adj->return_type == PRT_ANY)
			return 6;

		/* check any adjacent inputs which are pointing to this */
		/* top */
		if (socket_id > 7) {
			s = &sockets[socket_id - 8];
			for (i = 0; i < 4; ++i) {
				if (s->input_sides[i] == INPUT_SIDE_BOTTOM && s->return_type != socket->return_type)
					return 5;
			}
		}
	} else {
		enum piece_return_types it = pieces[socket->type].input_types[input];
		/* correct type */
		if (socket_adj->return_type != it) {
			D(printf("return type: %d. input type %d\n", socket_adj->return_type, it));
			return 4;
		}
	}

	return 0;
}

void
init_enemy_tactics()
{
	unsigned i, j;

	for (i = 0; i < nenemies; ++i)
		for (j = 0; j < 4; ++j)
			if (0 != enemies[i].presets[j].name[0])
				preset_to_graph(&enemies[i].presets[j]);
}

void
tactics_save()
{
	unsigned i, j;
	struct unit *unit = &heroes[tactics_hero_id];
	struct preset *preset = NULL;

	if (tactics_enemy_edit)
		unit = &enemies[tactics_enemy_id];

	preset = &unit->presets[unit->preset];

	/* @TODO: add other fields */
	for (i = 0; i < 64; ++i) {
		preset->sockets[i].type = sockets[i].type;
		preset->sockets[i].return_type = sockets[i].return_type;
		preset->sockets[i].state.p = 0;
		preset->sockets[i].state.q = 0;
		for (j = 0; j < 4; ++j)
			preset->sockets[i].input_sides[j] = sockets[i].input_sides[j];
	}
	D(printf("Saved preset '%s' with %d pieces\n", preset->name, preset->piece_count));
}

void
mouse_button_callback_tactics(GLFWwindow *window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		switch (action) {
		case GLFW_PRESS: {
			unsigned i;
			struct sprite *sprite;
			float left, right, top, bottom;
			unsigned start = (piece_page-1)*15 + 1;
			unsigned end = start + 15 > npieces ? npieces : start + 15;

			for (i = start; i < end; ++i) {
				sprite = &sprites[piece_sprite_ids[i]];

				left = sprite->x - (sprite->width / 2);
				right = sprite->x + (sprite->width / 2);
				top = sprite->y + (sprite->height / 2);
				bottom = sprite->y - (sprite->height / 2);

				if (cursor_x > left && cursor_x < right && cursor_y > bottom && cursor_y < top) {
					sprites[debug_square_id].x = sprite->x;
					sprites[debug_square_id].y = sprite->y;
					cursor_piece = i;
					selected_piece = i;
					selected_socket = -1;
					break;
				}
			}

			for (i = 0; i < 64; ++i) {
				struct socket *socket = &sockets[i];
				/* @NOTE: magic number 14 and 96 is referenced from the mockup image */

				left = socket->x - (socket->w / 2);
				right = socket->x + (socket->w / 2);
				top = socket->y + (socket->h / 2);
				bottom = socket->y - (socket->h / 2);

				if (cursor_x > left && cursor_x < right && cursor_y > bottom && cursor_y < top) {
					socket->input_sides[0] = INPUT_SIDE_NONE;
					socket->input_sides[1] = INPUT_SIDE_NONE;
					socket->input_sides[2] = INPUT_SIDE_NONE;
					socket->input_sides[3] = INPUT_SIDE_NONE;
					cursor_piece = sockets[i].type;
					insert_piece(0, &sockets[i]);
					selected_piece = 0;
					selected_socket = -1;
				}
			}

			/* click on input
			 * @TODO remove magic numbers
			 */
			{
			struct socket *socket = &sockets[selected_socket];
			struct piece *data = &pieces[socket->type];
			for (i = 0; i < data->input_count; ++i) {
				float x = INPUTS_X + 12;
				float y = INPUTS_Y - (i * 24);
				float cleft, cright, ctop, cbottom;
				left = x - 12;
				right = x + 12;
				top = y + 12;
				bottom = y - 12;

				/* set up 5 clickable areas
				 * centre, then clockwise top, right, bottom, left
				 */
				/* centre */
				cleft = left + 8;
				cright = right - 8;
				ctop = top - 8;
				cbottom = bottom + 8;
				if (cursor_x > cleft && cursor_x < cright && cursor_y > cbottom && cursor_y < ctop) {
					D(printf("centre input: %d\n", i));
					socket->input_sides[i] = INPUT_SIDE_NONE;
					break;
				}
				/* top */
				cleft = left + 8;
				cright = right - 8;
				ctop = top;
				cbottom = top - 8;
				if (cursor_x > cleft && cursor_x < cright && cursor_y > cbottom && cursor_y < ctop) {
					unsigned r = tactics_input_selection_check(selected_socket, i, INPUT_SIDE_TOP);
					D(printf("top input: %d, r: %u\n", i, r));
					if (0 == r)
						socket->input_sides[i] = INPUT_SIDE_TOP;
					break;
				}
				/* right */
				cleft = right - 8;
				cright = right;
				ctop = top - 8;
				cbottom = bottom + 8;
				if (cursor_x > cleft && cursor_x < cright && cursor_y > cbottom && cursor_y < ctop) {
					unsigned r = tactics_input_selection_check(selected_socket, i, INPUT_SIDE_RIGHT);
					D(printf("right input: %d, r: %u\n", i, r));
					if (0 == r)
						socket->input_sides[i] = INPUT_SIDE_RIGHT;
					break;
				}
				/* bottom */
				cleft = left + 8;
				cright = right - 8;
				ctop = bottom + 8;
				cbottom = bottom;
				if (cursor_x > cleft && cursor_x < cright && cursor_y > cbottom && cursor_y < ctop) {
					unsigned r = tactics_input_selection_check(selected_socket, i, INPUT_SIDE_BOTTOM);
					D(printf("bottom input: %d, r: %u\n", i, r));
					if (0 == r)
						socket->input_sides[i] = INPUT_SIDE_BOTTOM;
					break;
				}
				/* left */
				cleft = left;
				cright = left + 8;
				ctop = top - 8;
				cbottom = bottom + 8;
				if (cursor_x > cleft && cursor_x < cright && cursor_y > cbottom && cursor_y < ctop) {
					unsigned r = tactics_input_selection_check(selected_socket, i, INPUT_SIDE_LEFT);
					D(printf("left input: %d, r: %u\n", i, r));
					if (0 == r)
						socket->input_sides[i] = INPUT_SIDE_LEFT;
					break;
				}
			}
			}

			/* click on page  @TODO pagination*/
			for (i = 0; i < page_count; ++i) {
				float left = PAGINATION_X + 10 * i - 5;
				float bottom = PAGINATION_Y - 5;
				float right = left + 10;
				float top = bottom + 10;
				if (cursor_y > bottom && cursor_x > left && cursor_y < top && cursor_x < right) {
					piece_page = i + 1;
				}
			}
		} break;
		case GLFW_RELEASE: {
			unsigned i;
			float left, right, top, bottom;
			/* socket piece */
			/* @NOTE: 64 is from 8x8 grid */
			for (i = 0; i < 64; ++i) {
				struct socket *socket = &sockets[i];
				/* @NOTE: magic number 14 and 96 is referenced from the mockup image */

				left = socket->x - (socket->w / 2);
				right = socket->x + (socket->w / 2);
				top = socket->y + (socket->h / 2);
				bottom = socket->y - (socket->h / 2);

				if (cursor_piece && cursor_x > left && cursor_x < right && cursor_y > bottom && cursor_y < top) {
					insert_piece(cursor_piece, &sockets[i]);
					selected_socket = i;
					selected_piece = 0;
					break;
				}
 			}

			/* wipe any inputs pointing to this piece */
			clear_inputs(i);
			/* then automatically join any appropriate inputs */
			{
				struct socket *t, *s = &sockets[i];
				int x = i % 8, y = i / 8;
				unsigned j;
				for (j = 0; j < 4; ++j)
					s->input_sides[j] = INPUT_SIDE_NONE;
				if (x > 0) {
					t = &sockets[i - 1];
					for (j = 0; j < 4; ++j)
						if (pieces[s->type].input_types[j] == t->return_type
						    && INPUT_SIDE_NONE == s->input_sides[j]) {
							s->input_sides[j] = INPUT_SIDE_LEFT;
							break;
						}
				}
				if (y < 7) {
					t = &sockets[i + 8];
					for (j = 0; j < 4; ++j)
						if (pieces[s->type].input_types[j] == t->return_type
						    && INPUT_SIDE_NONE == s->input_sides[j]) {
							s->input_sides[j] = INPUT_SIDE_BOTTOM;
							break;
						}
				}
				if (y > 0) {
					t = &sockets[i - 8];
					for (j = 0; j < 4; ++j)
						if (pieces[s->type].input_types[j] == t->return_type
						    && INPUT_SIDE_NONE == s->input_sides[j]) {
							s->input_sides[j] = INPUT_SIDE_TOP;
							break;
						}
				}
				if (x < 7) {
					t = &sockets[i + 1];
					for (j = 0; j < 4; ++j)
						if (pieces[s->type].input_types[j] == t->return_type
						    && INPUT_SIDE_NONE == s->input_sides[j]) {
							s->input_sides[j] = INPUT_SIDE_RIGHT;
							break;
						}
				}
			}

			tactics_board_check();
 			tactics_save();
			cursor_piece = 0;
		} break;
		}
	}

	if (button == GLFW_MOUSE_BUTTON_RIGHT) {
		switch (action) {
		case GLFW_PRESS: {
			unsigned i;
			float left, right, top, bottom;
			/* socket piece */
			/* @NOTE: 64 is from 8x8 grid */
			for (i = 0; i < 64; ++i) {
				struct socket *socket = &sockets[i];
				/* @NOTE: magic number 14 and 96 is referenced from the mockup image */

				left = socket->x - (socket->w / 2);
				right = socket->x + (socket->w / 2);
				top = socket->y + (socket->h / 2);
				bottom = socket->y - (socket->h / 2);

				if (cursor_x > left && cursor_x < right && cursor_y > bottom && cursor_y < top) {
					insert_piece(0, &sockets[i]);
					clear_inputs(i);
				}
			}
			cursor_piece = 0;
			selected_piece = 0;
			selected_socket = -1;
		} break;
		}
	}
}

/* BATTLE =================================================================== */

/* unit status */
#define ALIVE 1
#define DEAD 0

/* probably not going to be more than 12 units on the board at once */
#define MAXUNITS 12
struct unit units[MAXUNITS] = {0};
unsigned state_unit_count = 0;

unsigned scene_battle_initialised = 0;
unsigned sprite_id_battle_board;

unsigned sprite_id_battle_sage;
unsigned sprite_id_battle_alchemist;
unsigned sprite_id_battle_bruiser;
unsigned sprite_id_battle_bat;

struct sprite *sprite_telegraph = NULL;
struct sprite *sprite_telegraph_player = NULL;
struct sprite *sprite_stun = NULL;
struct sprite *sprite_blind = NULL;
struct sprite *sprite_poison = NULL;

unsigned unit_turns[MAXUNITS];
unsigned unit_turn_count = 0;

unsigned turn = 1;

unsigned battle_set = 0;

unsigned state_target_id;

unsigned state_animating = 0;
struct unit *state_banim_unit = NULL;
void (*state_banim_cb)() = NULL;

unsigned telegraphs[64] = {0};

#define MAXBANIMS 32
struct battle_anim banims[MAXBANIMS] = {0};
unsigned nbanims;

#define BATTLE_GRID_TILE_ORIGIN_X SCR_WIDTH / 2
#define BATTLE_GRID_TILE_ORIGIN_Y SCR_HEIGHT - 42
#define BATTLE_GRID_TILE_WIDTH 24
#define BATTLE_GRID_TILE_HEIGHT 18
#define BATTLE_GRID_TILE_WIDTH_HALF 12
#define BATTLE_GRID_TILE_HEIGHT_HALF 9

#define CHARGING 0xfb
#define READY 0x26
#define BUSY 0xd0

struct unit *charge_hit_enemy = NULL;

double shake_start = 0;
double shake_end = 0;
double shakemx = 0;
double shakemy = 0;

struct dmg_txt {
	char s[32];
	float x;
	float y;
	float dx;
	float dy;
	double t;
	struct colour colour;
};

#define MAXDMGTXT 64
struct dmg_txt dmg_text[MAXDMGTXT] = {0};

void
battle_screen_shake_render()
{
	if (shake_start < time_now && shake_end > time_now) {
		shakex = rand() % 5 * shakemx;
		shakey = rand() % 5 * shakemy;
	} else {
		shakex = 0;
		shakey = 0;
	}

}

void
battle_screen_shake(float mx, float my, double d)
{
	shakemx = mx;
	shakemy = my;
	shake_start = time_now;
	shake_end = time_now + d;
}

void
battle_enemy_add(const char *strid, int x, int y)
{
	unsigned i;
	struct unit *tmpl = NULL;

	for (i = 0; i < nenemies; ++i)
		if (0 == strcmp(enemies[i].strid, strid))
			tmpl = &enemies[i];

	if (NULL == tmpl) {
		D(printf("cannot find enemy %s\n", strid));
		exit(10);
	} else {
		struct unit *unit = &units[state_unit_count++];
		*unit = *tmpl;
		unit->next_turn = 100;
		unit->status = ALIVE;
		unit->team = TEAM_ENEMY;
		unit->x = x;
		unit->y = y;
	}
}

void
battle_unit_add(struct sprite *sprite, int x, int y)
{
	D(printf("Added unit: %u\n", state_unit_count));
	units[state_unit_count].sprite = sprite;
	units[state_unit_count].x = x;
	units[state_unit_count].y = y;
	units[state_unit_count].status = ALIVE;
	units[state_unit_count].next_turn = 100;
	units[state_unit_count].team = 2;
	state_unit_count++;
}

void
battle_hero_add(struct unit *hero, unsigned sprite_id, int x, int y)
{
	unsigned i, j;
	struct unit *unit = &units[state_unit_count++];
	unit->sprite = &sprites[sprite_id];
	unit->x = x;
	unit->y = y;
	unit->status = ALIVE;
	unit->next_turn = 100;
	unit->team = TEAM_ALLY;
	for (i = 0; i < 3; ++i) {
		for (j = 0; j < 64; ++j) {
			unit->presets[i].sockets[j].input_sides[0] = hero->presets[i].sockets[j].input_sides[0];
			unit->presets[i].sockets[j].input_sides[1] = hero->presets[i].sockets[j].input_sides[1];
			unit->presets[i].sockets[j].input_sides[2] = hero->presets[i].sockets[j].input_sides[2];
			unit->presets[i].sockets[j].input_sides[3] = hero->presets[i].sockets[j].input_sides[3];
			unit->presets[i].sockets[j].type = hero->presets[i].sockets[j].type;
			unit->presets[i].sockets[j].return_type = pieces[hero->presets[i].sockets[j].type].return_type;
			unit->presets[i].sockets[j].state.p = hero->presets[i].sockets[j].state.p;
			unit->presets[i].sockets[j].state.q = hero->presets[i].sockets[j].state.q;
			unit->presets[i].sockets[j] = hero->presets[i].sockets[j];
			unit->presets[i].piece_order[j] = hero->presets[i].piece_order[j];
		}
		strcpy(unit->presets[i].name, hero->presets[i].name);
		unit->presets[i].piece_count = hero->presets[i].piece_count;

		strcpy(unit->name, hero->name);
		unit->hp = hero->hp;
		for (j = 0; j < 3; ++j) {
			unit->sets[j] = hero->sets[j];
			printf("setting set %d to %d\n", j, hero->sets[j]);
		}
		unit->spd = hero->spd;
		unit->ap = hero->ap;
		unit->preset = hero->preset;
	}
}

double turn_timer = 0;
double turn_len = 0.5f;

void
battle_end_turn()
{
	turn_timer = turn_len;
}

void
enemy_set_asprite(const char *s, const char *t)
{
	unsigned i;
	struct unit *u = NULL;
	struct sprite_animated *aspr = NULL;
	for (i = 0; i < nenemies; ++i)
		if (0 == strcmp(s, enemies[i].strid))
			u = &enemies[i];
	for (i = 0; i < nasprites; ++i)
		if (0 == strcmp(t, sprites_animated[i].strid))
			aspr = &sprites_animated[i];
	if (NULL == u || NULL == aspr) {
		D(printf("error setting enemy sprite %s %s\n", s, t));
		exit(1);
	} else {
		u->asprite = aspr;
	}
}

void
scene_battle_init()
{
	unsigned i;
	glfwSetMouseButtonCallback(window, mouse_button_callback_battle);
	glfwSetKeyCallback(window, key_callback_battle);

	for (i = 0; i < MAXUNITS; ++i) {
		struct unit *u = &units[i];
		u->status = DEAD;
	}

	state_unit_count = 0;
	state_animating = 0;
	turn = 0;

	/* @TODO: put heroes onto board */
	battle_hero_add(&heroes[0], sprite_id_battle_bruiser, 1, 1);
	battle_hero_add(&heroes[1], sprite_id_battle_alchemist, 4, 7);
	battle_hero_add(&heroes[2], sprite_id_battle_sage, 2, 6);

	/* @TODO: Add animated sprites to .dat somehow */
	enemy_set_asprite("bat", "bat");
	enemy_set_asprite("fatrat", "fatrat");
	enemy_set_asprite("rockguy", "rockguy");

	battle_enemy_add("bat", 6, 6);
	battle_enemy_add("rockguy", 6, 3);
	battle_enemy_add("fatrat", 7, 5);
}

void
map_to_screen(float *x, float *y, int mapx, int mapy)
{
	float fx = (float)mapx;
	float fy = (float)mapy;

	*x = BATTLE_GRID_TILE_ORIGIN_X + ((fx - fy) * BATTLE_GRID_TILE_WIDTH_HALF);
	*y = BATTLE_GRID_TILE_ORIGIN_Y - ((fx + fy) * BATTLE_GRID_TILE_HEIGHT_HALF);
}

void
screen_to_map(int *x, int *y, float posx, float posy)
{
	/* @TODO: The results are wrong... but we just offset by 16 and it works... */
	float fx = BATTLE_GRID_TILE_ORIGIN_X + posx;
	float fy = BATTLE_GRID_TILE_ORIGIN_Y - posy;

	*x = (int)(fx / BATTLE_GRID_TILE_WIDTH + fy / BATTLE_GRID_TILE_HEIGHT) - 16;
	*y = (int)(fy / BATTLE_GRID_TILE_HEIGHT - fx / BATTLE_GRID_TILE_WIDTH) + 16;
}

int
unit_id_from_unit(struct unit *u)
{
	unsigned i;
	for (i = 0; i < MAXUNITS; ++i) {
		if (u == &units[i])
			return i;
	}
	return -1;
}

struct unit *
unit_at_pos(int x, int y)
{
	unsigned i;
	for (i = 0; i < MAXUNITS; ++i) {
		struct unit *u = &units[i];
		if (u->x == x && u->y == y)
			return u;
	}
	return NULL;
}

unsigned
unit_at_pos_alive(int x, int y)
{
	struct unit *u = unit_at_pos(x, y);
	if (NULL == u || DEAD == u->status) {
		return 0;
	} else {
		return 1;
	}
}

unsigned
unit_has_status(struct unit *u, status_effect_e t)
{
	unsigned i;
	for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
		if (u->status_effects[i].turns > 0)
			if (u->status_effects[i].type == t)
				return 1;
	}
	return 0;
}

float
battle_dist_betweenf(float x, float y, float x2, float y2)
{
	return (float)sqrt(pow(x - x2, 2) + pow(y - y2, 2));
}

int
battle_dist_between(int x, int y, int x2, int y2)
{
	return (int)sqrt(pow(x - x2, 2) + pow(y - y2, 2));
}

void
damage_unit(struct unit *a, struct unit *u, int amount, enum dmg_t t)
{
	unsigned i;
	switch (t) {
	case DMG_PHS: amount -= (amount * u->pdf.val) / 100; break;
	case DMG_MAG: amount -= (amount * u->mdf.val) / 100; break;
	}
	for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
		struct status_effect *s = &u->status_effects[i];
		if (s->turns > 0) {
			switch (s->type) {
			case IMMUNE: return;
			case COVERED:
				damage_unit(NULL, u->unit_covering, amount, t);
				return;
			}
		}
	}

	if (NULL != a) {
		/* attaker effect */
		for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
			struct status_effect *s = &a->status_effects[i];
			if (s->turns > 0) {
				switch (s->type) {
				case BLIND:
					if (DMG_PHS == t) {
						unsigned j;
						for (j = 0; j < MAXDMGTXT; ++j) {
							if (0 >= dmg_text[j].t) {
								struct dmg_txt *dt = &dmg_text[i];
								map_to_screen(&dt->x, &dt->y, a->x, a->y);
								dt->dx = (rand() % 4) - 2;
								dt->dy = (rand() % 20);
								dt->t = 2.0;
								sprintf(dt->s, "%s", "MISS");
								dt->colour = colour_red;
							}
						}
						return;
					}
				}
			}
		}
	}

	u->hp.val -= amount;
	if (u->hp.val <= 0) {
		unsigned j;
		/* unit die */
		u->status = DEAD;
		for (j = 0; j < 64; ++j)
			if (u->telegraphs[j]) {
				telegraphs[j] = 0;
				u->telegraphs[j] = 0;
			}
	}
	for (i = 0; i < MAXDMGTXT; ++i) {
		if (0 >= dmg_text[i].t) {
			map_to_screen(&dmg_text[i].x, &dmg_text[i].y, u->x, u->y);
			dmg_text[i].dx = (rand() % 4) - 2;
			dmg_text[i].dy = (rand() % 20);
			dmg_text[i].t = 2.0;
			sprintf(dmg_text[i].s, "%d", amount);
			dmg_text[i].colour = t == DMG_PHS ? colour_white : colour_yellow;
			break;
		}
	}
	D(printf("%s takes %d damage!\n", u->name, amount));
}

unsigned
socket_get_neighbour(struct preset *preset, unsigned socket_id, unsigned side)
{
	switch (side) {
	case INPUT_SIDE_NONE:
		return socket_id;
	case INPUT_SIDE_TOP:
		if (socket_id < 8)
			break;
		return socket_id - 8;
	case INPUT_SIDE_RIGHT:
		if ((socket_id+1) % 8 == 0)
			break;
		return socket_id + 1;
	case INPUT_SIDE_BOTTOM:
		if (socket_id > 55)
			break;
		return socket_id + 8;
	case INPUT_SIDE_LEFT:
		if (socket_id % 8 == 0)
			break;
		return socket_id - 1;
	}
	return socket_id;
}

unsigned
unit_dead(struct unit *u)
{
	return DEAD == u->status;
}

unsigned
unit_dead_or_null(struct unit *u)
{
	return NULL == u || DEAD == u->status;
}

float
ease_linear(float x)
{
	return x + x;
}

float
ease_in_back(float x)
{
	float c1 = 1.70158f;
	float c3 = c1 + 1.f;
	return c3 * x * x * x - c1 * x * x;
}

float
ease_out_cubic(float x)
{
	return 1 - pow(1 - x, 3);
}

void
vec_dir_to(int *dx, int *dy, int x, int y, int x2, int y2)
{
	if (x < x2)
		*dx = 1;
	else if (x > x2)
		*dx = -1;
	else
		*dx = 0;

	if (y < y2)
		*dy = 1;
	else if (y > y2)
		*dy = -1;
	else
		*dy = 0;
}

void
cb_charge(struct battle_anim *a)
{
	struct unit *u = state_banim_unit;
	float x, y;
	int mapx = u->x, mapy = u->y;
	unsigned i;
	map_to_screen(&x, &y, mapx, mapy);
	x = x + u->ox;
	y = y + u->oy;
	screen_to_map(&mapx, &mapy, x, y);
	u->ox = 0;
	u->oy = 0;
	u->x = mapx;
	u->y = mapy;
	for (i = 0; i < 64; ++i) {
		if (u->telegraphs[i]) {
			u->telegraphs[i] = 0;
			telegraphs[i] = 0;
		}
	}
	u->charging = 0;
	battle_screen_shake(0.5, 0.5, 0.2);
	if (NULL != charge_hit_enemy) {
		int dx, dy;
		int newx, newy;
		vec_dir_to(&dx, &dy, u->x, u->y, charge_hit_enemy->x, charge_hit_enemy->y);
		newx = charge_hit_enemy->x + dx;
		newy = charge_hit_enemy->y + dy;
		if (int_in_range(newx, 0, 7) && int_in_range(newy, 0, 7) && !unit_at_pos_alive(newx + dx, newy + dy)) {
			charge_hit_enemy->x = newx;
			charge_hit_enemy->y = newy;
			damage_unit(u, charge_hit_enemy, 120, DMG_PHS);
		}
		if (unit_at_pos_alive(newx + dx, newy + dy) && unit_at_pos_alive(newx + dx * 2, newy + dy * 2)) {
			damage_unit(u, unit_at_pos(newx+dx, newy+dy), 80, DMG_PHS);
			damage_unit(u, unit_at_pos(newx+dx*2, newy+dy*2), 80, DMG_PHS);
		}
	}
}

/* @TODO: restructure how this works so we can use a switch
 */ 
void
perform_action(unsigned unit_id, unsigned socket_id)
{
	struct unit *unit = &units[unit_id];
	struct preset *preset = &unit->presets[unit->preset];
	struct socket *socket = &preset->sockets[socket_id];
	struct piece *p = &pieces[socket->type];
	struct socket *sb = &preset->sockets[socket_get_neighbour(preset, socket_id, socket->input_sides[1])];
	struct socket *sc = &preset->sockets[socket_get_neighbour(preset, socket_id, socket->input_sides[2])];

	D(printf("\033[0;32m[%d] %s -> %d %d\033[0;0m\n", socket_id, p->name, sb->state.p, sb->state.q));

	if (0 == strcmp("move.dat", p->file)) {
		int new_x = unit->x + sb->state.p;
		int new_y = unit->y + sb->state.q;
		struct unit *u = unit_at_pos(new_x, new_y);
		if (new_x >= 0 && new_x <= 7 && new_y >= 0 && new_y <= 7 && unit_dead_or_null(u)) {
			unit->x = new_x;
			unit->y = new_y;
			unit->ap.val -= 2;
		}
	} else if (0 == strcmp("attack.dat", p->file)) {
		int x = sb->state.p + unit->x;
		int y = sb->state.q + unit->y;
		struct unit *u = unit_at_pos(x, y);
		unit->ap.val -= 2;
		damage_unit(unit, u, 60, DMG_PHS);
	} else if (0 == strcmp("grapple.dat", p->file)) {
		struct unit *t = unit_at_pos(unit->x + sb->state.p, unit->y + sb->state.q);
		if (unit_at_pos_alive(unit->x + sb->state.p, unit->y + sb->state.q)) {
			unsigned i = 0;
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 == t->status_effects[i].turns) {
					printf("SETTING %s to STUN\n", t->name);
					t->status_effects[i].type = STUN;
					t->status_effects[i].turns = 2;
					break;
				}
			}
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 == unit->status_effects[i].turns) {
					printf("SETTING %s to GRAPPLE\n", unit->name);
					unit->status_effects[i].type = GRAPPLE;
					unit->status_effects[i].turns = 2;
					break;
				}
			}
			unit->ap.val -= pieces[socket->type].ap;
		}
	} else if (0 == strcmp("cover.dat", p->file)) {
		struct unit *t = unit_at_pos(unit->x + sb->state.p, unit->y + sb->state.q);
		if (unit_at_pos_alive(unit->x + sb->state.p, unit->y + sb->state.q)) {
			unsigned i = 0;
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 == t->status_effects[i].turns) {
					printf("SETTING %s to COVERED\n", t->name);
					t->status_effects[i].type = COVERED;
					t->status_effects[i].turns = 2;
					t->unit_covering = unit;
					break;
				}
			}
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 == unit->status_effects[i].turns) {
					printf("SETTING %s to COVERING\n", unit->name);
					unit->status_effects[i].type = COVERING;
					unit->status_effects[i].turns = 2;
					break;
				}
			}
			unit->ap.val -= pieces[socket->type].ap;
		}
	} else if (0 == strcmp("corrosive_vial.dat", p->file)) {
		int x = unit->x + sb->state.p * sc->state.p;
		int y = unit->y + sb->state.q * sc->state.p;
		printf("throwing vial at %d %d (%s)\n", x, y, unit_at_pos(x, y)->name);
		if (unit_at_pos_alive(x, y)) {
			struct unit *u = unit_at_pos(x, y);
			unsigned i;
			damage_unit(unit, u, 30, DMG_MAG);
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 >= u->status_effects[i].turns) {
					u->status_effects[i].type = POISON;
					u->status_effects[i].turns = 10;
					break;
				}
			}
		}
	} else if (0 == strcmp("blinding_gas.dat", p->file)) {
		int x = unit->x + sb->state.p * sc->state.p;
		int y = unit->y + sb->state.q * sc->state.p;
		printf("blinding gas at %d %d (%s)\n", x, y, unit_at_pos(x, y)->name);
		if (unit_at_pos_alive(x, y)) {
			struct unit *u = unit_at_pos(x, y);
			unsigned i;
			for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
				if (0 >= u->status_effects[i].turns) {
					u->status_effects[i].type = BLIND;
					u->status_effects[i].turns = 2;
					break;
				}
			}
		}
	} else if (0 == strcmp("align_axis.dat", p->file)) {
		struct unit *u = &units[sb->state.p];
		if (abs(unit->x) - abs(u->x) > abs(unit->y) - abs(u->y)) {
			if (u->y - unit->y >= 0) {
				if (unit->y < 7)
					if (!unit_at_pos_alive(unit->x, unit->y+1))
						unit->y++;
			} else {
				if (unit->y > 0)
					if (!unit_at_pos_alive(unit->x, unit->y-1))
						unit->y--;
			}
		} else {
			if (u->x - unit->x >= 0) {
				if (unit->x < 7)
					if (!unit_at_pos_alive(unit->x+1, unit->y))
						unit->x++;
			} else {
				if (unit->x > 0)
					if (!unit_at_pos_alive(unit->x-1, unit->y))
						unit->x--;
			}
		}
	} else if (0 == strcmp("charge.dat", p->file)) {
		if (CHARGING == unit->charging) {
			if (unit->charge_turn <= unit->turn)
				unit->charging = READY;
		} else if (0 == unit->charging) {
			int n = 5;
			int mapx = unit->x;
			int mapy = unit->y;
			int p = sb->state.p;
			int q = sb->state.q;
			unit->charging = CHARGING;
			unit->charge_turn = unit->turn+2;
			while (n-- > 0) {
				if (int_in_range(mapx + p, 0, 7) && int_in_range(mapy + q, 0, 7)) {
					mapx += p;
					mapy += q;
					telegraphs[8 * mapy + mapx] = 1;
					unit->telegraphs[8 * mapy + mapx] = 1;
				} else 
					break;
			}
			unit->p = p;
			unit->q = q;
		}

		if (READY == unit->charging) {
			/* add animation to queue */
			int n = 5;
			int mapx = unit->x;
			int mapy = unit->y;
			int p = unit->p;
			int q = unit->q;
			float sx, sy, tx, ty, dx, dy, d;
			double dur, end;


			/* lets say we are gonna charge 5 tiles or until an edge or collision */
			/* @TODO: the reason it is jittery is because it never exists in the first square, kind of */
			while (n-- > 0) {
				if (int_in_range(mapx + p, 0, 7) && int_in_range(mapy + q, 0, 7) && !unit_at_pos_alive(mapx + p, mapy + q)) {
					mapx += p;
					mapy += q;
				} else 
					break;
			}

			if (unit_at_pos_alive(mapx + p, mapy + q))
				charge_hit_enemy = unit_at_pos(mapx + p, mapy + q);
			else
				charge_hit_enemy = NULL;

			map_to_screen(&sx, &sy, unit->x, unit->y);
			map_to_screen(&tx, &ty, mapx, mapy);
			dx = tx - sx;
			dy = ty - sy;
			state_animating = 1;
			unit->charging = BUSY;

			d = battle_dist_betweenf(sx, sy, tx, ty);
			dur = d * 0.01;

			banims[0].delta = dx;
			banims[0].easing = ease_in_back;
			banims[0].dur = dur;
			banims[0].startval = unit->ox;
			banims[0].val = &unit->ox;
			banims[0].start = time_now;

			banims[1].delta = dy;
			banims[1].easing = ease_in_back;
			banims[1].dur = dur;
			banims[1].startval = unit->oy;
			banims[1].val = &unit->oy;
			banims[1].start = time_now;

			nbanims = 2;
			state_banim_unit = unit;
			state_banim_cb = cb_charge;

			end = fmax2(banims[0].start + banims[0].dur, banims[1].start + banims[1].dur);
		}
	}
}

void
evaluate_socket(unsigned unit_id, unsigned socket_id)
{
	struct unit *unit = &units[unit_id];
	struct preset *preset = &unit->presets[unit->preset];
	struct socket *socket = &preset->sockets[socket_id];
	struct piece *p = &pieces[socket->type];
	struct socket *sa = &preset->sockets[socket_get_neighbour(preset, socket_id, socket->input_sides[0])];
	struct socket *sb = &preset->sockets[socket_get_neighbour(preset, socket_id, socket->input_sides[1])];

	if (PRT_NONE == p->return_type) {
		socket->state.p = sa->state.p;
	} else if (0 == strcmp("0.dat", p->file)) {
		socket->state.p = 0;
	} else if (0 == strcmp("1.dat", p->file)) {
		socket->state.p = 1;
	} else if (0 == strcmp("2.dat", p->file)) {
		socket->state.p = 2;
	} else if (0 == strcmp("3.dat", p->file)) {
		socket->state.p = 3;
	} else if (0 == strcmp("4.dat", p->file)) {
		socket->state.p = 4;
	} else if (0 == strcmp("5.dat", p->file)) {
		socket->state.p = 5;
	} else if (0 == strcmp("6.dat", p->file)) {
		socket->state.p = 6;
	} else if (0 == strcmp("7.dat", p->file)) {
		socket->state.p = 7;
	} else if (0 == strcmp("8.dat", p->file)) {
		socket->state.p = 8;
	} else if (0 == strcmp("9.dat", p->file)) {
		socket->state.p = 9;
	} else if (0 == strcmp("add.dat", p->file)) {
		socket->state.p = sa->state.p + sb->state.p;
	} else if (0 == strcmp("adj.dat", p->file)) {
		struct unit *a = &units[sa->state.p];
		struct unit *b = &units[sb->state.p];
		socket->state.p = ((a->x == b->x && abs(a->y - b->y) == 1) || ((a->y == b->y) && abs(a->x - b->x) == 1));
	} else if (0 == strcmp("and.dat", p->file)) {
		socket->state.p = (sa->state.p == 1 && sb->state.p == 1);
	} else if (0 == strcmp("closest_enemy.dat", p->file)) {
		unsigned i;
		int d = 99;
		int id = -1;
		for (i = 0; i < MAXUNITS; ++i) {
			struct unit *u = &units[i];
			if (!unit_dead_or_null(u))
				if (u->team == TEAM_ENEMY) {
					int d2 = battle_dist_between(unit->x, unit->y, u->x, u->y);
					if (d2 < d) {
						id = i;
						d = d2;
					}
				}
		}
		socket->state.p = id == sa->state.p;
	} else if (0 == strcmp("connector.dat", p->file)) {
		socket->state.p = sa->state.p;
		socket->state.q = sa->state.q;
	} else if (0 == strcmp("dir_rand.dat", p->file)) {
		int dir = rand() % 4;
		int x, y;
		switch (dir) {
		case 0: x = 0; y = 1; break;
		case 1: x = 1; y = 0; break;
		case 2: x = 0; y = -1; break;
		case 3: x = -1; y = 0; break;
		}
		socket->state.p = x;
		socket->state.q = y;
	} else if (0 == strcmp("dir_to.dat", p->file)) {
		struct unit *a = &units[sa->state.p];
		int x = a->x - unit->x;
		int y = a->y - unit->y;
		int p = 0, q = 0;
		/* need to account for negatives */
		if (x > 0) p = 1;
		else if (x < 0) p = -1;
		if (y > 0) q = 1;
		else if (y < 0) q = -1;

		if (x != 0 && y != 0) {
			if (abs(x) > abs(y))
				q = 0;
			else
				p = 0;
		}

		socket->state.p = p;
		socket->state.q = q;
	} else if (0 == strcmp("dist_to.dat", p->file)) {
		struct unit *a = &units[sa->state.p];
		int d = battle_dist_between(unit->x, unit->y, a->x, a->y);
		socket->state.p = d;
	} else if (0 == strcmp("div.dat", p->file)) {
		socket->state.p = sa->state.p / sb->state.p;
	} else if (0 == strcmp("enemy_team.dat", p->file)) {
		socket->state.p = TEAM_ENEMY;
	} else if (0 == strcmp("eq.dat", p->file)) {
		socket->state.p = (sa->state.p == sb->state.p);
	} else if (0 == strcmp("false.dat", p->file)) {
		socket->state.p = 0;
	} else if (0 == strcmp("gt.dat", p->file)) {
		socket->state.p = sa->state.p > sb->state.p;
	} else if (0 == strcmp("gte.dat", p->file)) {
		socket->state.p = sa->state.p >= sb->state.p;
	} else if (0 == strcmp("highest_hp.dat", p->file)) {
		unsigned i;
		int d = 0;
		int id = -1;
		for (i = 0; i < MAXUNITS; ++i) {
			struct unit *u = &units[i];
			if (DEAD == unit->status || i == unit_id || sb->state.p != u->team) continue;
			if (u->hp.val > d) {
				d = u->hp.val;
				id = i;
			}
		}
		socket->state.p = id == sa->state.p;
	} else if (0 == strcmp("hp.dat", p->file)) {
		socket->state.p = unit->hp.val;
	} else if (0 == strcmp("id.dat", p->file)) {
		socket->state.p = sa->state.p;
	} else if (0 == strcmp("lowest_hp.dat", p->file)) {
		unsigned i;
		int d = 99999999;
		int id = -1;
		for (i = 0; i < MAXUNITS; ++i) {
			struct unit *u = &units[i];
			if (DEAD == unit->status || i == unit_id || sb->state.p != u->team) continue;
			if (u->hp.val < d) {
				d = u->hp.val;
				id = i;
			}
		}
		socket->state.p = id == sa->state.p;
	} else if (0 == strcmp("lt.dat", p->file)) {
		socket->state.p = sa->state.p < sb->state.p;
	} else if (0 == strcmp("lte.dat", p->file)) {
		socket->state.p = sa->state.p <= sb->state.p;
	} else if (0 == strcmp("mhp.dat", p->file)) {
		socket->state.p = unit->hp.max;
	} else if (0 == strcmp("mod.dat", p->file)) {
		socket->state.p = sa->state.p % sb->state.p;
	} else if (0 == strcmp("mul.dat", p->file)) {
		socket->state.p = sa->state.p * sb->state.p;
	} else if (0 == strcmp("neq.dat", p->file)) {
		socket->state.p = sa->state.p != sb->state.p;
	} else if (0 == strcmp("not.dat", p->file)) {
		socket->state.p = !sa->state.p;
	} else if (0 == strcmp("or.dat", p->file)) {
		socket->state.p = sa->state.p || sb->state.p;
	} else if (0 == strcmp("raycast.dat", p->file)) {
		int x = unit->x + sa->state.p;
		int y = unit->y + sa->state.q;
		if (sa->state.p == 0 && sa->state.q == 0) {
			socket->state.p = 0;
		} else {
			struct unit *u = unit_at_pos(x, y);
			while (x <= 7 && x >= 0 && y <= 7 && y >= 0) {
				D(printf("checking tile %d %d...\n", x, y));
				u = unit_at_pos(x, y);
				if (NULL != u && unit_at_pos_alive(x, y)) {
					int id = unit_id_from_unit(u);
					printf("Found unit at %d %d with id %d\n", x, y, id);
					socket->state.p = id;
					break;
				}
				x += sa->state.p;
				y += sa->state.q;
			}
			if (NULL == u)
				socket->state.p = 0;
		}
	} else if (0 == strcmp("self.dat", p->file)) {
		socket->state.p = unit_id;
	} else if (0 == strcmp("south.dat", p->file)) {
		socket->state.p = 0;
		socket->state.q = 1;
	} else if (0 == strcmp("sub.dat", p->file)) {
		socket->state.p = sa->state.p - sb->state.p;
	} else if (0 == strcmp("target.dat", p->file)) {
		socket->state.p = state_target_id;
	} else if (0 == strcmp("team.dat", p->file)) {
		socket->state.p = units[sa->state.p].team;
	} else if (0 == strcmp("true.dat", p->file)) {
		socket->state.p = 1;
	} else if (0 == strcmp("unit_vector.dat", p->file)) {
		socket->state.p = unit->x;
		socket->state.q = unit->y;
	} else if (0 == strcmp("vector_new.dat", p->file)) {
		socket->state.p = sa->state.p;
		socket->state.q = sa->state.q;
	} else if (0 == strcmp("vector_x.dat", p->file)) {
		socket->state.p = sa->state.p;
	} else if (0 == strcmp("vector_y.dat", p->file)) {
		socket->state.p = sa->state.q;
	}
}

struct piece *
piece_from_name(const char *name)
{
	unsigned i;
	for (i = 0; i < MAXPIECES; ++i)
		if (strcmp(pieces[i].name, name) == 0)
			return &pieces[i];
	return NULL;
}

struct piece *
piece_from_id(unsigned id)
{
	unsigned i;
	for (i = 0; i < MAXPIECES; ++i)
		if (pieces[i].id == id)
			return &pieces[i];
	return NULL;
}

unsigned
battle_take_turn(unsigned unit_id)
{
	unsigned i, j, k;
	struct unit *unit = &units[unit_id];
	struct preset *preset;
	unsigned targets[MAXUNITS] = {0};
	unsigned ntargets = 0;
	unsigned actions[64] = {0};
	unsigned nactions = 0;

	unit->preset = unit->sets[battle_set];
	preset = &unit->presets[unit->preset];

	D(printf("\033[0;33m== Take Turn: %s... ", unit->name)); 

	/* no turn if dead! */
	if (DEAD == unit->status)
		return 0;

	/* check status effects */
	for (i = 0; i < MAXSTATUSEFFECTS; ++i) {
		struct status_effect *e = &unit->status_effects[i];
		if (e->turns > 0) {
			e->turns--;
			switch (e->type) {
			case STUN:
				printf("%s is stunned!\n", unit->name);
				return 0;
			case GRAPPLE:
				printf("%s is grappling...!\n", unit->name);
				break;
			case BLIND:
				printf("%s is blind...!\n", unit->name);
				break;
			case IMMUNE:
				break;
			case POISON:
				printf("%s is poisoned!\n", unit->name);
				damage_unit(NULL, unit, unit->hp.max / 20, DMG_MAG);
				break;
			case COVERING:
				printf("%s is covering, cannot take turn rn\n", unit->name);
				return 0;
			case COVERED:
				printf("%s is covered, cannot take turn rn\n", unit->name);
				return 0;
			}
		}
	}

	/* get action pieces */
	for (i = 0; i < preset->piece_count; ++i) {
		if (preset->sockets[preset->piece_order[i]].return_type == PRT_NONE)
			actions[nactions++] = preset->piece_order[i];
	}

	D(printf("pieces: %d, actions: %d\033[0;0m\n", preset->piece_count, nactions)); 

	/* build target list */
	for (i = 0; i < MAXUNITS; ++i)
		if (ALIVE == units[i].status)
			targets[ntargets++] = i;

	/* if the condition for action 1 is true on ANY target, then do action 1
	 * that means we must check every target against the action sequence which leads to action 1
	 * so we need to...
	 * for each action, evaluate all non action pieces
	 * if the trigger for that piece resolves to true against any target, perform the action
	 * eg: current action is BITE. can we bite target 1? can we bite target 2? etc
	 */

	for (i = 0; i < nactions; ++i) {
		unsigned asid = actions[i];
		struct socket *action = &preset->sockets[asid];
		for (j = 0; j < ntargets; ++j) {
			state_target_id = targets[j];
			for (k = 0; k < preset->piece_count; ++k) {
				unsigned sid = preset->piece_order[k];
				evaluate_socket(unit_id, sid);
			}
			if (1 == action->state.p)
				if (unit->ap.val >= piece_from_id(action->type)->ap)
					perform_action(unit_id, asid);
		}
	}

	unit->ap.val = unit->ap.max;
	unit->turn++;

	battle_end_turn();
	return 0;
}

void
battle_turn()
{
	unsigned i;
	unsigned allies_alive = 0;
	unsigned enemies_alive = 0;
	D(printf("\033[0;36mTurn: %u, Unit Count: %u\033[0;0m\n", turn++, state_unit_count));

	for (i = 0; i < MAXUNITS; ++i) {
		if (!unit_dead_or_null(&units[i])) {
			if (TEAM_ALLY == units[i].team)
				allies_alive++;
			else
				enemies_alive++;
		}
	}

	if (!allies_alive) {
		victory = 1;
		switch_scene(scene_game_over);
		return;
	}

	if (!enemies_alive) {
		victory = 0;
		switch_scene(scene_game_over);
		return;
	}


	for (i = 0; i < state_unit_count; ++i) {
		struct unit *unit = &units[i];
		/* printf("Unit selected: %s spd: %u\n", unit->name, unit->spd); */
		unit->next_turn -= unit->spd.val;
		if (unit->next_turn <= 0) {
			unsigned delay = battle_take_turn(i);
			unit->next_turn = 100 + delay;
		}
	}
	turn++;
}

void
battle_animate()
{
	/* play all anims until they are done... */
	unsigned i;
	double end = 0;
	for (i = 0; i < nbanims; ++i) {
		struct battle_anim *a = &banims[i];
		end = fmax2(end, banims[i].start + banims[i].dur);
		if (time_now > a->start && time_now < a->start + a->dur) {
			float p = (time_now - a->start) / a->dur;
			*a->val = a->startval + a->delta * a->easing(p);
		} else if (time_now > end)
			*a->val = a->startval + a->delta;
	}
	if (time_now > end) {
		if (NULL != state_banim_cb)
			state_banim_cb();
		state_animating = 0;
		nbanims = 0;
	}
}

void
scene_battle()
{
	unsigned i;
	char turn_txt[32] = {0};
	struct sprite *sprite = &sprites[sprite_id_battle_board];
	sprite->x = SCR_WIDTH / 2;
	sprite->y = SCR_HEIGHT / 2;
	render_sprite(sprite);

	for (i = 0; i < 64; ++i) {
		int x = i % 8, y = i / 8;
		if (0 == telegraphs[i])
			continue;
		else if (1 == telegraphs[i])
			sprite = sprite_telegraph;
		else if (2 == telegraphs[i])
			sprite = sprite_telegraph_player;
		map_to_screen(&sprite->x, &sprite->y, x, y);
		sprite->y++;
		render_sprite(sprite);
	}

	turn_timer -= delta_time;
	if (0 >= turn_timer) {
		if (state_animating)
			battle_animate();
		else
			battle_turn();
	}

	for (i = 0; i < MAXUNITS; ++i) {
		struct sprite_animated *asprite = units[i].asprite;
		if (DEAD == units[i].status)
			continue;
		sprite = units[i].sprite;
		map_to_screen(&sprite->x, &sprite->y, units[i].x, units[i].y);
		sprite->x += units[i].ox;
		sprite->y += units[i].oy;
		units[i].fx = sprite->x;
		units[i].fy = sprite->y;

		sprite->y += sprite->height / 2;

		if (NULL == asprite)
			render_sprite(sprite);
		else {
			struct sprite_animation *anim = &asprite->animations[asprite->index];
			render_sprite_animated(asprite, sprite->x, sprite->y, 0, 0);
			asprite->timer -= delta_time;
			if (asprite->timer <= 0) {
				anim->index++;
				if (anim->index >= anim->length)
					anim->index = 0;
				asprite->timer = anim->durations[anim->index];
			}
		}

		tint_r = tint_g = tint_b = tint_a = 1;

	}

	for (i = 0; i < MAXUNITS; ++i) {
		struct unit *u = &units[i];
		if (unit_dead_or_null(u))
			continue;
		if (unit_has_status(u, BLIND)) {
			sprite = sprite_blind;
			map_to_screen(&sprite->x, &sprite->y, u->x, u->y);
			sprite->x += u->ox;
			sprite->y += u->oy;
			sprite->y += sprite->height / 2;
			render_sprite(sprite);
		}
		if (unit_has_status(u, STUN)) {
			sprite = sprite_stun;
			map_to_screen(&sprite->x, &sprite->y, u->x, u->y);
			sprite->x += u->ox;
			sprite->y += u->oy;
			sprite->y += sprite->height / 2;
			render_sprite(sprite);
		}
		if (unit_has_status(u, POISON)) {
			sprite = sprite_poison;
			map_to_screen(&sprite->x, &sprite->y, u->x, u->y);
			sprite->x += u->ox;
			sprite->y += u->oy;
			sprite->y += sprite->height / 2;
			render_sprite(sprite);
		}
	}

	sprintf(turn_txt, "Turn: %d", turn);
	render_text(turn_txt, &fonts[1], 4, 4, colour_white);

	for (i = 0; i < MAXDMGTXT; ++i) {
		struct dmg_txt *d = &dmg_text[i];
		if (d->t > 0) {
			d->t -= delta_time;
			d->x += d->dx * delta_time;
			d->y += d->dy * delta_time;
			/* render_text(n, &fonts[3], d->x, d->y - 0.4, colour_black);*/
			render_text(d->s, &fonts[2], d->x, d->y, d->colour);
		}
	}

	battle_screen_shake_render();
}

void
battle_cycle_presets()
{
	battle_set++;
	if (battle_set > 2)
		battle_set = 0;
}

void
mouse_button_callback_battle(GLFWwindow *window, int button, int action, int mods)
{
}

void
key_callback_battle(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS && key == GLFW_KEY_SPACE)
		battle_cycle_presets();
}

/* PRESETS ================================================================== */

unsigned scene_presets_initialised = 0;
float preset_btn_x = 75;
float preset_btn_y = SCR_HEIGHT - 32;
float preset_btn_width = 53;
float preset_btn_height = 20;
float preset_btn_margin_x = 4;
float preset_btn_margin_y = 4;

void
key_callback_presets(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS && key == GLFW_KEY_D)
		switch_scene(scene_battle);
}

void
mouse_button_callback_presets(GLFWwindow *window, int button, int action, int mods)
{
	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT: {
		switch (action) {
		case GLFW_PRESS: {
			unsigned i, j;
			float sx = preset_btn_x - preset_btn_width / 2;
			float sy = preset_btn_y - preset_btn_height / 2;
			float x, y, x2, y2;
			for (i = 0; i < 3; ++i) {
				x = sx + i * (preset_btn_width + preset_btn_margin_x);
				x2 = x + preset_btn_width;
				for (j = 0; j < 3; ++j) {
					y = sy - j * (preset_btn_height + preset_btn_margin_y);
					y2 = y + preset_btn_height;
					if (cursor_x > x && cursor_x < x2 && cursor_y > y && cursor_y < y2) {
						heroes[i].sets[j]++;
						if (heroes[i].sets[j] > 2)
							heroes[i].sets[j] = 0;
					}
				}
			}
		} break;
		}
	} break;
	}
}

void
scene_presets_init()
{
	glfwSetMouseButtonCallback(window, mouse_button_callback_presets);
	glfwSetKeyCallback(window, key_callback_presets);

	if (scene_presets_initialised)
		return;
	scene_presets_initialised = 1;
}

void
scene_presets()
{
	struct sprite *sprite;
	unsigned i, j;
	const char *sets[3] = { "Set 1", "Set 2", "Set 3" };

	for (i = 0; i < 3; ++i) {
		for (j = 0; j < 3; ++j) {
			struct preset *preset = &heroes[i].presets[heroes[i].sets[j]];
			/* dont use name, may be good spot for the strategy pattern */
			if (strcmp(preset->name, "Preset A") == 0) {
				sprite = &sprites[sprite_id_button_preset_a];
			} else if (strcmp(preset->name, "Preset B") == 0) {
				sprite = &sprites[sprite_id_button_preset_b];
			} else if (strcmp(preset->name, "Preset C") == 0) {
				sprite = &sprites[sprite_id_button_preset_c];
			}
			sprite->x = preset_btn_x + i * (preset_btn_width + preset_btn_margin_x);
			sprite->y = preset_btn_y - j * (preset_btn_height + preset_btn_margin_y);
			render_sprite(sprite);
		}
		render_text(heroes[i].name, &fonts[0], preset_btn_x + i * (preset_btn_width + preset_btn_margin_x) - preset_btn_width / 2, SCR_HEIGHT - 20, colour_white);
		render_text(sets[i], &fonts[0], 15, SCR_HEIGHT - 32 - (i * (preset_btn_height + preset_btn_margin_y)), colour_white);
	}

	/* render presets button...
	 * menu -> tactics -> presets -> battle 
	 */
	sprite = &sprites[sprite_id_button_done];
	sprite->x = 334;
	sprite->y = 20;
	render_sprite(sprite);
}

/* GAME_OVER ================================================================ */

void
key_callback_game_over(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (GLFW_PRESS == action) {
		switch (key) {
		case GLFW_KEY_SPACE:
			switch_scene(scene_tactics);
			break;
		}
	}
}

void
scene_game_over_init()
{
	glfwSetKeyCallback(window, key_callback_game_over);
}

void
scene_game_over()
{
	render_text("By Dylan Falconer", &fonts[0], 150, SCR_HEIGHT - 50, colour_white);
	render_text("Additional Credits", &fonts[0], 150, SCR_HEIGHT - 60, colour_white);
	render_text("Roboto Font by Christian Robertson", &fonts[1], 150, SCR_HEIGHT - 65, colour_white);
	render_text("Image Loading Library (stb_image) by Sean Barrett", &fonts[1], 150, SCR_HEIGHT - 70, colour_white);
	render_text("OpenGL Library (GLFW) by Camilla Lowy, Jonas Adahl, linkmuave and Siavash Eliasi", &fonts[1], 150, SCR_HEIGHT - 75, colour_white);
	render_text("OpenGL Loader (glad) by David Herberth", &fonts[1], 150, SCR_HEIGHT - 80, colour_white);
	render_text("Press Esc to exit. Press Space to try again.", &fonts[1], 150, SCR_HEIGHT - 120, colour_yellow);
}

void
cursor_position_callback(GLFWwindow *window, double xpos, double ypos)
{
	cursor_x = xpos / SCR_SCALE;
	cursor_y = SCR_HEIGHT - ypos / SCR_SCALE;
}

void
switch_scene(void (*new_scene)())
{
	scene = new_scene;

	if (scene == scene_tactics) {
		scene_tactics_init();
	} else if (scene == scene_battle) {
		scene_battle_init();
	} else if (scene == scene_presets) {
		scene_presets_init();
	} else if (scene == scene_game_over) {
		scene_game_over_init();
	}
}

void
fgetst(char *s, int l, FILE *f)
{
	unsigned len;
	fgets(s, l, f);
	len = strlen(s);
	if (0 == len)
		D(printf("Tried to read a 0 length string.\n"));
	if (s[len-1] == '\n')
		s[len-1] = 0;
}

void
fgetl(char *s, int l, FILE *f)
{
	int c;
	int i;

	for (i = l-1; i > 0; --i)
		s[i] = 0;

	do {
		c = fgetc(f);
		if ('\n' == c)
			s[i++] = 0;
		else
			s[i++] = c;
	} while (c != EOF && c != '\n');
}

enum piece_return_types
prt_from_str(char *s)
{
	if      (0 == strcmp("NONE", s))		return PRT_NONE;
	else if (0 == strcmp("ANY", s))			return PRT_ANY;
	else if (0 == strcmp("BOOLEAN", s))		return PRT_BOOLEAN;
	else if (0 == strcmp("NUMBER", s))		return PRT_NUMBER;
	else if (0 == strcmp("UNIT", s))		return PRT_UNIT;
	else if (0 == strcmp("VECTOR", s))		return PRT_VECTOR;
	else if (0 == strcmp("VECTOR_LIST", s))		return PRT_VECTOR_LIST;
	else {
		D(printf("INVALID RETURN TYPE %s\n", s));
		exit(1);
	}
}

int
fnpiececmp(const void *a, const void *b)
{
	struct piece *pa = (struct piece *)a;
	struct piece *pb = (struct piece *)b;
	return strcmp(pa->file, pb->file);
}

void
data_set_stat(struct unit *u, char *s, char *t)
{
	int val = atoi(t);

	if (0 == strcmp(s, "hp")) {
		u->hp.val = val;
		u->hp.max = val;
	} else if (0 == strcmp(s, "ap")) {
		u->ap.val = val;
		u->ap.max = val;
	} else if (0 == strcmp(s, "spd")) {
		u->spd.val = val;
		u->spd.max = val;
	} else if (0 == strcmp(s, "pdf")) {
		u->pdf.val = val;
		u->pdf.max = val;
	} else if (0 == strcmp(s, "mdf")) {
		u->mdf.val = val;
		u->mdf.max = val;
	} else {
		D(printf("Error during data import. %s is not a valid stat./n", s));
		return;
	}
}

struct preset *
preset_by_str(const char *s)
{
	unsigned i;

	for (i = 0; i < npresets; ++i)
		if (0 == strcmp(presets[i].file, s))
			return &presets[i];

	return NULL;
}

struct piece *
piece_from_str(const char *s)
{
	unsigned i;
	char file[32];
	for (i = 0; i < npieces; ++i) {
		strcpy(file, pieces[i].file);
		file[strlen(file)-4] = 0;
		if (0 == strcmp(file, s))
			return &pieces[i];
	}

	return NULL;
}

void
data_init()
{
	/* pieces */
	{
		/* read piece data */
		unsigned i;
		struct dirent *dir;
		DIR *d= opendir("./data/pieces");
		char ls[45] = "Reading piece files...\n";
		if (d == NULL) {
			D(printf("Could not open dir\n"));
			exit(1);
		}

		D(printf("%-45s", ls));

		while ((dir = readdir(d)) != NULL) {
			char path[128] = "./data/pieces/";
			FILE *f;
			char name[32];
			char desc[128];
			char img[128];
			char ret[16];
			char ap[3];
			char input_names[4][32] = {0};
			char input_types[4][32] = {0};
			unsigned ninputs = 0;
			unsigned id;
			unsigned sprite_id;
			if ('.' == dir->d_name[0]) {
				D(printf("Skipping '%s'\n", dir->d_name));
			} else {
				strcpy(ls, "Reading file ");
				strcat(ls, dir->d_name);
				D(printf("%-45s", ls));
				id = npieces++;
				f = fopen(strcat(path, dir->d_name), "r");
				fgetst(name, 32, f);
				fgetst(desc, 128, f);
				fgetst(img, 128, f);
				fgetst(ret, 16, f);

				if (0 == strcmp("NONE", ret))
					fgetst(ap, 3, f);

				for (i = 0; i < 4; ++i) {
					fscanf(f, "%s", input_names[i]);
					fscanf(f, "%s", input_types[i]);
					if ('\0' != input_names[i][0])
						ninputs++;
				}

				/* add piece? */
				strcpy(pieces[id].name, name);
				strcpy(pieces[id].desc, desc);
				strcpy(pieces[id].img, img);
				strcpy(pieces[id].file, dir->d_name);
				pieces[id].id = id;
				sprite_id = sprite_add(img);
				pieces[id].sprite = &sprites[sprite_id];
				pieces[id].sprite_id = sprite_id;
				pieces[id].return_type = prt_from_str(ret);
				if (0 == strcmp("NONE", ret))
					pieces[id].ap = atoi(ap);

				for (i = 0; i < ninputs; ++i) {
					strcpy(pieces[id].input_names[i], input_names[i]);
					pieces[id].input_types[i] = prt_from_str(input_types[i]);
				}

				pieces[id].input_count = ninputs;
				D(printf("\033[0;32m%5s\033[0m\n", "OK!"));
			}
		}

		closedir(d);

		/* sort! */
		qsort(pieces, npieces, sizeof(struct piece), fnpiececmp);

		for (i = 0; i < npieces; ++i) {
			struct piece *piece = &pieces[i];
			piece_sprite_ids[i] = piece->sprite_id;
			piece->id = i;
			if (0 == strcmp(pieces[i].file, "none.dat")) {
				struct piece zero = pieces[0];
				struct piece none = pieces[i];
				pieces[0] = none;
				pieces[0].id = 0;
				pieces[i] = zero;
				pieces[i].id = zero.id;
				piece_sprite_ids[0] = none.sprite_id;
				piece_sprite_ids[i] = zero.sprite_id;
			}
			/*
			D(printf("piece: %s %s\n", piece->name, piece->img));
			D(printf("inputs: "));
			D(for (j = 0; j < piece->input_count; ++j))
				D(printf("%s %d, ", piece->input_names[j], piece->input_types[j]));
			D(printf("\n"));
			*/
		}
	}

	/* presets */
	{
		struct dirent *dir;
		DIR *d = opendir("./data/presets");
		char ls[45];
		if (NULL == d) {
			D(printf("Could not open dir\n"));
			exit(1);
		}

		D(puts("Reading preset files..."));

		while (NULL != (dir = readdir(d))) {
			char path[128] = "./data/presets/";
			FILE *f;
			unsigned id;
			struct preset *preset;
			char slot[4];
			char info[32];
			struct piece *piece;
			if ('.' == dir->d_name[0]) {
				D(printf("Skipping '%s'\n", dir->d_name));
			} else {
				strcpy(ls, "Reading file ");
				strcat(ls, dir->d_name);
				D(printf("%-45s", ls));
				id = npresets++;
				preset = &presets[id];
				strcpy(preset->file, dir->d_name);
				f = fopen(strcat(path, dir->d_name), "r");
				fgetst(preset->name, 32, f);

				while (0 == feof(f)) {
					int c;
					char pname[32];
					unsigned i = 1;
					fscanf(f, "%s", slot);
					fgetst(info, 32, f);
					D(printf("info: %s\n", info));
					/* @TODO: bug where last piece must have a space after */
					c = info[i];
					while (' ' != c && '\n' != c && EOF != c) {
						/* printf(">%d<\n", i); */
						pname[i-1] = c;
						c = info[++i];
					}
					pname[i-1] = 0;
					piece = piece_from_str(pname);
					if (piece != NULL) {
						struct socket *socket = &preset->sockets[atoi(slot)];
						unsigned side = 0;
						socket->type = piece->id;
						socket->return_type = piece->return_type;
						/* inputs */
						i = 0;
						while (0 != c) {
							c = info[i++];
							switch (c) {
							case 'T':
								socket->input_sides[side++] = INPUT_SIDE_TOP;
								break;
							case 'R':
								socket->input_sides[side++] = INPUT_SIDE_RIGHT;
								break;
							case 'B':
								socket->input_sides[side++] = INPUT_SIDE_BOTTOM;
								break;
							case 'L':
								socket->input_sides[side++] = INPUT_SIDE_LEFT;
								break;
							}
						}
					}
				}

				D(printf("\033[0;32m%5s\033[0m\n", "OK!"));
			}
		}
	}

	/* enemies */
	{
		/* read enemy data */
		struct dirent *dir;
		DIR *d = opendir("./data/enemies");
		char ls[45];
		if (NULL == d) {
			D(printf("Could not open dir\n"));
			exit(1);
		}

		D(puts("Reading enemy files..."));

		/* ap, status, hp, name, next_turn, preset, presets, sets, spd, sprite, team, x, y */
		while (NULL != (dir = readdir(d))) {
			char path[128] = "./data/enemies/";
			FILE *f;
			unsigned id;
			struct unit *enemy;
			char team[3];
			unsigned sprite_id;
			char tmp[128];
			char d[2] = " ";
			char *tok;
			int n = 0;
			if ('.' == dir->d_name[0]) {
				D(printf("Skipping '%s'\n", dir->d_name));
			} else {
				strcpy(ls, "Reading file ");
				strcat(ls, dir->d_name);
				D(printf("%-45s", ls));
				id = nenemies++;
				enemy = &enemies[id];
				f = fopen(strcat(path, dir->d_name), "r");
				strcpy(enemy->strid, dir->d_name);
				enemy->strid[strlen(dir->d_name)-4] = 0;
				fgetst(enemy->name, 32, f);
				fgetst(team, 3, f);
				enemy->team = atoi(team);
				fgetst(enemy->img, 128, f);
				sprite_id = sprite_add(enemy->img);
				enemy->sprite = &sprites[sprite_id];
				enemy->sprite_id = sprite_id;
				/* preset files */
				fgetst(tmp, 128, f);
				tok = strtok(tmp, d);
				while (NULL != tok) {
					enemy->presets[n++] = *preset_by_str(tok);
					tok = strtok(NULL, d);
				}

				/* stats */
				while (0 == feof(f)) {
					char line[32];
					char *stat;
					char *val;
					fgetl(line, 32, f);
					if (EOF == line[0])
						break;
					stat = strtok(line, d);
					val = strtok(NULL, d);
					D(printf("[%s][%s:%s]\n", line, stat, val));
					data_set_stat(enemy, stat, val);
				}

				D(printf("\033[0;32m%5s\033[0m\n", "OK!"));
			}
		}
	}
	init_enemy_tactics();

	/* sprites */
	{
		debug_square_id = sprite_add("assets/debug_square.png");
		piece_highlight_id = sprite_add("assets/piece_selected_overlay.png");
		sprite_id_input_base = sprite_add("assets/input_base.png");
		sprite_id_input_blue_base = sprite_add("assets/input_blue_base.png");
		sprite_id_input_red_base = sprite_add("assets/input_red_base.png");
		sprite_id_input_green_base = sprite_add("assets/input_green_base.png");
		sprite_id_input_blue = sprite_add("assets/input_blue_left.png");
		sprite_id_input_red = sprite_add("assets/input_red_left.png");
		sprite_id_input_green = sprite_add("assets/input_green_left.png");
		sprite_id_input_arrow_blue = sprite_add("assets/input_arrow_blue.png");
		sprite_id_input_arrow_red = sprite_add("assets/input_arrow_red.png");
		sprite_id_input_arrow_green = sprite_add("assets/input_arrow_green.png");
		sprite_id_tactics_buttons = sprite_add("assets/piece_buttons.png");
		sprite_id_pagination_1 = sprite_add("assets/piece_page_tab_1.png");
		sprite_id_pagination_1_selected = sprite_add("assets/piece_page_tab_1_selected.png");
		sprite_id_pagination_2 = sprite_add("assets/piece_page_tab_2.png");
		sprite_id_pagination_2_selected = sprite_add("assets/piece_page_tab_2_selected.png");
		sprite_id_pagination_3 = sprite_add("assets/piece_page_tab_3.png");
		sprite_id_pagination_3_selected = sprite_add("assets/piece_page_tab_3_selected.png");
		sprite_id_pagination_4 = sprite_add("assets/piece_page_tab_4.png");
		sprite_id_pagination_4_selected = sprite_add("assets/piece_page_tab_4_selected.png");
		sprite_id_pagination_5 = sprite_add("assets/piece_page_tab_5.png");
		sprite_id_pagination_5_selected = sprite_add("assets/piece_page_tab_5_selected.png");
		sprite_id_button_battle = sprite_add("assets/battle.png");
		sprite_id_button_presets = sprite_add("assets/presets.png");
		sprite_id_button_done = sprite_add("assets/done.png");

		sprite_id_battle_board = sprite_add("assets/board.png");
		sprite_id_battle_sage = sprite_add("assets/sage.png");
		sprite_id_battle_alchemist = sprite_add("assets/alchemist.png");
		sprite_id_battle_bruiser = sprite_add("assets/bruiser.png");
		sprite_id_battle_bat = sprite_add("assets/bat.png");
		sprite_telegraph = &sprites[sprite_add("assets/telegraph.png")];
		sprite_telegraph_player = &sprites[sprite_add("assets/telegraph_player.png")];
		sprite_stun = &sprites[sprite_add("assets/stun.png")];
		sprite_blind = &sprites[sprite_add("assets/blind.png")];
		sprite_poison = &sprites[sprite_add("assets/poison.png")];

		sprite_id_button_preset_a = sprite_add("assets/preset_a.png");
		sprite_id_button_preset_b = sprite_add("assets/preset_b.png");
		sprite_id_button_preset_c = sprite_add("assets/preset_c.png");
	}

}

struct sprite_animated *
asprite_add(const char *s)
{
	struct sprite_animated *aspr = &sprites_animated[nasprites++];
	strcpy(aspr->strid, s);
	return aspr;
}

struct sprite_animated *
aspr_by_name(const char *s)
{
	unsigned i;
	for (i = 0; i < nasprites; ++i) {
		if (0 == strcmp(s, sprites_animated[i].strid))
			return &sprites_animated[i];
	}
	return NULL;
}

void
asprite_anim_set(const char *s, const char *p, float *d, unsigned n)
{
	unsigned i;
	char fullpath[128];
	struct sprite_animated *aspr = aspr_by_name(s);
	struct sprite_animation *anim;
	if (NULL == aspr) {
		D(printf("cannot find aspr %s\n", s));
		exit(1);
	} else {
		anim = &aspr->animations[aspr->nanims++];
		for (i = 0; i < n; ++i) {
			anim->durations[i] = d[i];
			sprintf(fullpath, "assets/%s%d.png", p, i+1);
			anim->indices[i] = sprite_add(fullpath);
		}
		anim->length = n;
		free(d);
	}
}

void
asprite_anim_add(struct sprite_animated *asprite, const char *path, float *durations, unsigned len)
{
	unsigned i;
	char fullpath[128];
	struct sprite_animation *anim = &asprite->animations[asprite->nanims++];
	for (i = 0; i < len; ++i) {
		anim->durations[i] = durations[i];
		sprintf(fullpath, "assets/%s%d.png", path, i+1);
		anim->indices[i] = sprite_add(fullpath);
	}
	anim->length = len;
	free(durations);
}

float *
va_floats(int n, ...)
{
	unsigned i;
	float *d = malloc(n * sizeof(float));
	va_list ap;
	va_start(ap, n);
	for (i = 0; i < n; ++i)
		d[i] = (float)va_arg(ap, double);
	va_end(ap);
	return d;
}

void
tests()
{
	unsigned i = 64;
	while (i-- > 0) {
		int x = i % 8;
		int y = i / 8;
		float p, q;
		int s, t;
		map_to_screen(&p, &q, x, y);
		printf("__map_to_screen %d %d -> %.0f %.0f\n", x, y, p, q);
		screen_to_map(&s, &t, p, q);
		printf("__screen_to_map %.0f %.0f -> %d %d\n", p, q, s, t);
	}
}

int
main(void)
{
	srand(time(NULL));

	font_add("assets/fonts/Roboto.ttf", 40, 1.2f);
	font_add("assets/fonts/Roboto.ttf", 16, 1.2f);
	font_add("assets/fonts/Roboto.ttf", 28, 1.2f);

	renderer_init();

	data_init();
	tests();

	/* setup animated sprites */
	{
		asprite_add("bat");
		asprite_anim_set("bat", "bat_idle", va_floats(4, 0.1f, 0.1f, 0.1f, 0.1f), 4);
		asprite_add("fatrat");
		asprite_anim_set("fatrat", "fatrat_idle", va_floats(5, 0.3f, 0.4f, 0.2f, 0.3f), 4);
		asprite_add("rockguy");
		asprite_anim_set("rockguy", "rockguy_idle", va_floats(5, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f), 5);
	}


	/* init state */
	strcpy(heroes[0].name, "Dismas"); /* Alchemist */
	strcpy(heroes[1].name, "Reynauld"); /* Bruiser */
	strcpy(heroes[2].name, "Lusaan"); /* Sage */
	strcpy(heroes[0].presets[0].name, "Preset A");
	strcpy(heroes[0].presets[1].name, "Preset B");
	strcpy(heroes[0].presets[2].name, "Preset C");
	strcpy(heroes[1].presets[0].name, "Preset A");
	strcpy(heroes[1].presets[1].name, "Preset B");
	strcpy(heroes[1].presets[2].name, "Preset C");
	strcpy(heroes[2].presets[0].name, "Preset A");
	strcpy(heroes[2].presets[1].name, "Preset B");
	strcpy(heroes[2].presets[2].name, "Preset C");
	heroes[0].hp.max = 500;
	heroes[0].hp.val = 500;
	heroes[1].hp.max = 400;
	heroes[1].hp.val = 400;
	heroes[2].hp.max = 400;
	heroes[2].hp.val = 400;
	heroes[0].spd.max = 30;
	heroes[0].spd.val = 30;
	heroes[1].spd.max = 22;
	heroes[1].spd.val = 22;
	heroes[2].spd.max = 12;
	heroes[2].spd.val = 12;
	heroes[0].ap.max = 4;
	heroes[0].ap.val = 4;
	heroes[1].ap.max = 4;
	heroes[1].ap.val = 4;
	heroes[2].ap.max = 4;
	heroes[2].ap.val = 4;

	preset_set_by_filename(&heroes[0].presets[0], "h_bruiser.dat");
	preset_set_by_filename(&heroes[0].presets[1], "h_bruiser2.dat");
	preset_set_by_filename(&heroes[0].presets[2], "h_bruiser3.dat");
	preset_to_graph(&heroes[0].presets[0]);
	preset_to_graph(&heroes[0].presets[1]);
	preset_to_graph(&heroes[0].presets[2]);
	preset_set_by_filename(&heroes[1].presets[0], "h_alchemist.dat");
	preset_set_by_filename(&heroes[1].presets[1], "h_alchemist2.dat");
	preset_to_graph(&heroes[1].presets[0]);
	preset_to_graph(&heroes[1].presets[1]);
	preset_set_by_filename(&heroes[2].presets[0], "h_alchemist.dat");
	preset_to_graph(&heroes[2].presets[0]);

	switch_scene(scene_tactics);
	
	while (!glfwWindowShouldClose(window)) {
		time_now = glfwGetTime();
		delta_time = time_now - time_last_frame;
		process_input(window);
		glClear(GL_COLOR_BUFFER_BIT);

		/* start render */
		scene();
		/* end render */

		glfwSwapBuffers(window);
		glfwPollEvents();
		time_last_frame = time_now;
	}

	glfwTerminate();
	return 0;
}

#include "ash.h"

#define SCR_WIDTH 384
#define SCR_HEIGHT 216
#define SCR_SCALE 5
#define CLEAR_COLOUR 0.1f, 0.1f, 0.1f, 1.0f

GLFWwindow *window;

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

int main(void)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

	glfwSetErrorCallback(error_callback);

	window = glfwCreateWindow(SCR_WIDTH * SCR_SCALE, SCR_HEIGHT * SCR_SCALE, "TEST", NULL, NULL);

	assert(window);

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	assert(0 != gladLoadGLLoader((GLADloadproc)glfwGetProcAddress));

	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);

		/* start render */
		/* end render */

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

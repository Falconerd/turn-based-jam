CC = gcc
STD = c89
LDFLAGS = -Wall -pedantic -ansi
LIBS = -lglfw -lm -ldl -lfreetype
INC = /usr/include

WIN_CC = x86_64-w64-mingw32-gcc
WIN_LIBS = -lglfw3dll -lm -lfreetype
WIN_LIB_DIR = ./lib
WIN_INC_DIR = ./include

debug: src/main.c lib/glad.c
	$(CC) -g3 $(LDFLAGS) -I $(INC) $^ $(LIBS) -std=$(STD) -o ./a.out
nix: src/main.c lib/glad.c
	$(CC) $(LDFLAGS) -I $(INC) $^ $(LIBS) -std=$(STD) -o ./a.out
win: src/main.c lib/glad.c
	$(WIN_CC) -L $(WIN_LIB_DIR) -I $(WIN_INC_DIR) $^ $(WIN_LIBS) -std=$(STD) -o ./a.exe
